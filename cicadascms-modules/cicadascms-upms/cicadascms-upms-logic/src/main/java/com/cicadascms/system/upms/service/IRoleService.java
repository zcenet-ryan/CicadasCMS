package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.RoleDO;
import com.cicadascms.system.upms.dto.RoleInputDTO;
import com.cicadascms.system.upms.dto.RoleQueryDTO;
import com.cicadascms.system.upms.dto.RoleUpdateDTO;
import com.cicadascms.system.upms.vo.RoleVO;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author westboy
 * @date 2019-07-10
 */
public interface IRoleService extends IService<RoleDO> {

    List<RoleDO> findByUserId(Serializable userId);

    List<RoleVO>  getTree();

    List<RoleVO> findList(RoleQueryDTO roleQueryDTO);

    /**
     * 分页方法
     * @param roleQueryDTO
     * @return
     */
    R page(RoleQueryDTO roleQueryDTO);

    /**
     * 保存方法
     * @param roleInputDTO
     * @return
     */
    R save(RoleInputDTO roleInputDTO);

    /**
     * 更新方法
     * @param roleUpdateDTO
     * @return
     */
    R update(RoleUpdateDTO roleUpdateDTO);

    /**
     * 查询方法
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     * @param id
     * @return
     */
    R deleteById(Serializable id);

}
