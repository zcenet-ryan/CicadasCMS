package com.cicadascms.system.upms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.QuartzJobDO;
import com.cicadascms.data.domain.QuartzJobLogDO;
import com.cicadascms.system.upms.dto.QuartzJobInputDTO;
import com.cicadascms.system.upms.dto.QuartzJobLogQueryDTO;
import com.cicadascms.system.upms.dto.QuartzJobQueryDTO;
import com.cicadascms.system.upms.dto.QuartzJobUpdateDTO;
import com.cicadascms.system.upms.service.IQuartzJobService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * <p>
 * 定时任务 控制器
 * </p>
 *
 * @author jin
 * @since 2020-04-29
 */
@Tag(name = "定时任务管理接口")
@RestController
@RequestMapping("/system/quartzJob")
public class QuartzController {
    @Autowired
    private IQuartzJobService quartzJobService;

    @ApiOperation(value = "定时任务列表接口")
    @GetMapping("/list")
    public R<Page<QuartzJobDO>> getPage(QuartzJobQueryDTO quartzJobQueryDTO) {
        return quartzJobService.page(quartzJobQueryDTO);
    }

    @ApiOperation(value = "定时任务执行日志列表接口")
    @GetMapping("/logs")
    public R<Page<QuartzJobLogDO>> processLogPage(QuartzJobLogQueryDTO quartzJobLogQueryDTO) {
        return quartzJobService.processLogPage(quartzJobLogQueryDTO);
    }


    @ApiOperation(value = "定时任务保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid QuartzJobInputDTO quartzJobInputDTO) throws Exception {
        return quartzJobService.save(quartzJobInputDTO);
    }

    @ApiOperation(value = "定时任务更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid QuartzJobUpdateDTO quartzJobUpdateDTO) throws Exception {
        return quartzJobService.update(quartzJobUpdateDTO);
    }

    @ApiOperation(value = "定时任务详情接口")
    @GetMapping("/{id}")
    public R<QuartzJobDO> getById(@PathVariable Long id) {
        return quartzJobService.findById(id);
    }

    @ApiOperation(value = "定时任务删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) throws Exception {
        return quartzJobService.deleteById(id);
    }

    @ApiOperation(value = "恢复定时任务")
    @GetMapping("/{id}/resume")
    public R<Boolean> resumeJob(@PathVariable Long id) throws Exception {
        return quartzJobService.resumeJob(id);
    }

    @ApiOperation(value = "暂停定时任务")
    @GetMapping("/{id}/pause")
    public R<Boolean> pauseJob(@PathVariable Long id) throws Exception {
        return quartzJobService.pauseJob(id);
    }

    @ApiOperation(value = "触发定时任务")
    @GetMapping("/{id}/trigger")
    public R<Boolean> triggerJob(@PathVariable Long id) throws Exception {
        return quartzJobService.triggerJob(id);
    }


    @ApiOperation(value = "立即执行")
    @GetMapping("/{id}/run")
    public R<Boolean> run(@PathVariable Long id) throws Exception {
        return quartzJobService.processJob(id);
    }
}
