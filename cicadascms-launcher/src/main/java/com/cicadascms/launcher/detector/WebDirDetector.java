/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.detector;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ClassUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Objects;

@Slf4j
public class WebDirDetector {

    public static boolean isStartupFromJar() {
        String protocol = Objects.requireNonNull(WebDirDetector.class.getResource("")).getProtocol();
        return Objects.equals(protocol, "jar");
    }

    public static boolean isStartupFromContainer() {
        String libDir = getLibDir(Objects.requireNonNull(ClassUtils.getDefaultClassLoader()));
        return !isStartupFromJar() && (libDir.contains("WEB-INF") && libDir.contains("lib"));
    }

    public static String getLibDir(ClassLoader classLoader) {
        URL url = classLoader.getResource("com/cicadascms/launcher/CicadasCmsLauncher.class");

        String file;

        try {
            assert url != null;
            file = new URI(url.getPath()).getPath();
        } catch (URISyntaxException uriSyntaxException) {
            file = url.getFile();
        }

        if (log.isDebugEnabled()) {
            log.debug("Lib url " + file);
        }

        int pos = file.indexOf("/com/cicadascms/launcher/");

        String libDir = file.substring(0, pos + 1);

        if (libDir.endsWith("/WEB-INF/classes/")) {
            libDir = libDir.substring(0, libDir.length() - 8) + "lib/";
        } else {
            pos = libDir.indexOf("/WEB-INF/lib/");

            if (pos != -1) {
                libDir = libDir.substring(0, pos) + "/WEB-INF/lib/";
            }
        }

        if (libDir.startsWith("file:/")) {
            libDir = libDir.substring(5);
        }

        return libDir;
    }

    public static String getRootDir(ClassLoader classLoader) {
        return getRootDir(getLibDir(classLoader));
    }

    public static String getRootDir(String libDir) {
        String rootDir = libDir;

        if (rootDir.endsWith("/WEB-INF/lib/")) {
            rootDir = rootDir.substring(0, rootDir.length() - 12);
        }

        return rootDir;
    }


}
