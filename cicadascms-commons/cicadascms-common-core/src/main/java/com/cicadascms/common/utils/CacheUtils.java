package com.cicadascms.common.utils;

import com.cicadascms.common.func.Fn;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

/**
 * CacheUtils
 *
 * @author Jin
 */
public class CacheUtils {
    private static final RedisTemplate<String, String> redisTemplate;

    static {
        redisTemplate = SpringContextUtils.getBean(StringRedisTemplate.class);
    }


    /**
     * 清除缓存
     *
     * @param cacheName
     * @param cacheKey
     */
    public static void clearCache(String cacheName, String cacheKey) {
        redisTemplate.boundHashOps(cacheName).delete(cacheKey);
    }

    /**
     * 设置缓存
     *
     * @param cacheName
     * @param cacheKey
     * @param o
     */
    public static void putCache(String cacheName, String cacheKey, Object o) {
        if (Fn.isNotEmpty(cacheKey) && Fn.isNotNull(o)) {
            redisTemplate.boundHashOps(cacheName).put(cacheKey, Fn.toJson(o));
        }
    }

    /**
     * 获取缓存对象
     *
     * @param cacheName
     * @param cacheKey
     * @param clazz
     * @param <T>
     * @return T
     */
    public static <T> T getCacheForObject(String cacheName, String cacheKey, Class<T> clazz) {
        if (Boolean.TRUE.equals(redisTemplate.hasKey(cacheName))) {
            String cacheValue = (String) redisTemplate.boundHashOps(cacheName).get(cacheKey);
            return Fn.fromJson(cacheValue, clazz);
        }
        return null;
    }

    /**
     * 获取缓存列表
     *
     * @param cacheName
     * @param cacheKey
     * @param clazz
     * @param <T>
     * @return List<T>
     */
    public static <T> List<T> getCacheForList(String cacheName, String cacheKey, Class<T> clazz) {
        if (Boolean.TRUE.equals(redisTemplate.hasKey(cacheKey))) {
            String cacheValue = (String) redisTemplate.boundHashOps(cacheName).get(cacheKey);
            return Fn.fromJsonList(cacheValue, clazz);
        }
        return null;
    }

}

