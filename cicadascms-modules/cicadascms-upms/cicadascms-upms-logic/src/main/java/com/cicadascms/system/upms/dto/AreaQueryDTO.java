package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.AreaDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * AreaQueryDTO对象
 * 区域
 * </p>
 *
 * @author jin
 * @since 2020-09-06
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "AreaQueryDTO对象")
public class AreaQueryDTO extends BaseDTO<AreaQueryDTO, AreaDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 父编号
     */
    @Schema(title = "4-父编号")
    private String parentId;
    /**
     * 级别
     */
    @Schema(title = "6-级别")
    private Integer levelType;


    public static Converter<AreaQueryDTO, AreaDO> converter = new Converter<AreaQueryDTO, AreaDO>() {
        @Override
        public AreaDO doForward(AreaQueryDTO areaQueryDTO) {
            return WarpsUtils.copyTo(areaQueryDTO, AreaDO.class);
        }

        @Override
        public AreaQueryDTO doBackward(AreaDO area) {
            return WarpsUtils.copyTo(area, AreaQueryDTO.class);
        }
    };

    @Override
    public AreaDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public AreaQueryDTO convertFor(AreaDO area) {
        return converter.doBackward(area);
    }
}
