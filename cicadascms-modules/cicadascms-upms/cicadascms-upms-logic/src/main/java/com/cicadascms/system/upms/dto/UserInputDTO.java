package com.cicadascms.system.upms.dto;


import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.UserDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * UserInputDTO对象
 * 基础账户表
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="InputUserDTO对象")
public class UserInputDTO extends BaseDTO<UserInputDTO, UserDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 邮箱
    */
        @Schema(title = "邮箱" )
    private String email;
    /**
    * 手机号
    */
        @Schema(title = "手机号" )
    private String phone;
    /**
    * 登陆名
    */
        @Schema(title = "账号名")
    private String username;
    /**
    * 密码
    */
        @Schema(title = "密码" )
    private String password;
    /**
    * 用户类型（1.管理员，2.注册会员）
    */
        @Schema(title = "用户类型（1.管理员，2.注册会员）" )
    private Integer userType;
    /**
    * 租户id
    */
        @Schema(title = "租户id" )
    private String tenantId;
    /**
    * 状态 1:enable, 0:disable, -1:deleted
    */
        @Schema(title = "状态 1:enable, 0:disable, -1:deleted" )
    private Integer status;

        @Schema(title = "真实姓名" )
    private String realName;

        @Schema(title = "头像" )
    private String avatar;

    /**
     * 角色编号
     */
        @Schema(title = "角色编号" )
    private String selectedRoleIdsStr;

    /**
     * 职位编号
     */
        @Schema(title = "职位编号" )
    private String selectedPostIdsStr;

    /**
     * 部门编号
     */
        @Schema(title = "部门编号" )
    private String selectedDeptIdsStr;


    public List<String> getRoleIdList() {
        return Fn.str2List(selectedRoleIdsStr);
    }

    public List<String> getPostIdList() {
        return Fn.str2List(selectedPostIdsStr);
    }

    public List<String> getDeptIdList() {
        return Fn.str2List(selectedDeptIdsStr);
    }


    public static Converter<UserInputDTO, UserDO> converter = new Converter<UserInputDTO, UserDO>() {
        @Override
        public UserDO doForward(UserInputDTO userInputDTO) {
            return WarpsUtils.copyTo(userInputDTO, UserDO.class);
        }

        @Override
        public UserInputDTO doBackward(UserDO user) {
            return WarpsUtils.copyTo(user, UserInputDTO.class);
        }
    };

    @Override
    public UserDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public UserInputDTO convertFor(UserDO user) {
        return converter.doBackward(user);
    }
}
