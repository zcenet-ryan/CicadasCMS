package com.cicadascms.website.front.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.data.domain.SiteDO;

/**
 * <p>
 * 站点表 服务类
 * </p>
 *
 * @author jin
 * @since 2020-10-10
 */
public interface ISiteService extends IService<SiteDO> {
    /**
     * 根据域名查询
     *
     * @param domain
     * @return
     */
    SiteDO findByDomain(String domain);

    /**
     * 根据dir查询
     *
     * @param dir
     * @return
     */
    SiteDO findBySiteDir(String dir);

    /**
     * 获取默认站点
     *
     * @return
     */
    SiteDO getDefaultSite();

}
