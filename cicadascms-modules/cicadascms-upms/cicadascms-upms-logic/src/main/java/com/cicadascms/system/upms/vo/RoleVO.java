package com.cicadascms.system.upms.vo;

import com.cicadascms.common.tree.TreeNode;
import com.cicadascms.data.domain.RoleDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Schema(title = "RoleVo", description = "角色")
@Data
@EqualsAndHashCode(callSuper = true)
public class RoleVO extends RoleDO implements TreeNode<RoleVO> {


    private String parentName;

    private List<RoleVO> children;

    @Schema(title = "选中的")
    private Integer[] selectedPermissionIds;

    @Override
    public Serializable getCurrentNodeId() {
        return getRoleId();
    }

    @Override
    public Serializable getParentNodeId() {
        return getParentId();
    }

}
