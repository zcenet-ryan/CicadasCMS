/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.builder.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.ImageView;

/**
 * TableInfoVo
 *
 * @author Jin
 */
public class TableInfo {

    private final StringProperty tableName;

    private final StringProperty tableComment;

    private final StringProperty engine;

    private final StringProperty createTime;

    private ImageView imageView;

    public String getTableName() {
        return tableName.get();
    }

    public StringProperty tableNameProperty() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName.set(tableName);
    }

    public String getTableComment() {
        return tableComment.get();
    }

    public StringProperty tableCommentProperty() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment.set(tableComment);
    }

    public String getEngine() {
        return engine.get();
    }

    public StringProperty engineProperty() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine.set(engine);
    }

    public String getCreateTime() {
        return createTime.get();
    }

    public StringProperty createTimeProperty() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime.set(createTime);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public TableInfo() {
        this.tableName = new SimpleStringProperty();
        this.tableComment = new SimpleStringProperty();
        this.engine = new SimpleStringProperty();
        this.createTime = new SimpleStringProperty();
    }
}
