CicadasCMS 2.0 开发中... 老版本请看master分支

QQ交流群:1482196

#### 项目结构

```
CicadasCMS v2.0
    ├── cicadascms-builder                              
    ├── cicadascms-commons                 
            ├── cicadascms-common-core                  
            ├── cicadascms-common-datasource            
            ├── cicadascms-common-logger               
            ├── cicadascms-common-lucene                
            ├── cicadascms-common-mybatis               
            ├── cicadascms-common-plugin                
            ├── cicadascms-common-redis                 
            ├── cicadascms-common-security            
            └── cicadascms-common-weixin                
    ├── cicadascms-data                    
            ├── cicadascms-domian                      
            └── cicadascms-mapper                      
    ├── cicadascms-launcher                           
    ├── cicadascms-modules                   
            ├── cicadascms-website-admin                        
                    ├── cicadascms-website-admin-app                
                    ├── cicadascms-website-admin-logic                
                    └── cicadascms-website-admin-static                 
            ├── cicadascms-website-front                       
                    ├── cicadascms-website-front-app                 
                    ├── cicadascms-website-front-logic                 
                    └── cicadascms-website-front-static                 
            └── cicadascms-system-upms                      
                    ├── cicadascms-system-upms-app                 
                    ├── cicadascms-system-upms-logic                 
                    └── cicadascms-system-upms-static  
    ├── cicadascms-support                               
    ├── cicadascms-ui                                      
    └── doc                       
``` 


#### 界面预览

![后台登录界面](docs/picture/login.png "后台登录界面")
![后台站点管理界面](docs/picture/site.png "后台站点管理界面")
![后台数据模型界面](docs/picture/model.png "后台数据模型界面")
![后台数据模型编辑界面](docs/picture/datamodel-field.png "后台数据模型编辑界面")
![后台部门管理界面](docs/picture/dept.png "后台部门管理界面")
![后台字典管理界面](docs/picture/dict.png "后台字典管理界面")
![后台角色管理界面](docs/picture/role.png "后台角色管理界面")
![后台定时任务管理界面](docs/picture/quartz.png "后台定时任务管理界面")
![后台令牌列表界面](docs/picture/token-list.png "后台令牌列表界面")
![代码生成器界面](docs/picture/generator.png "代码生成器界面")

<img src="https://img.shields.io/badge/JDK-1.8+-green.svg" alt="jdk version">
<img src="https://img.shields.io/badge/Spring%20Boot-2.4.2-blue.svg" alt="Coverage Status">
<img src="https://img.shields.io/badge/Mybatis%20Plus-3.4.2-red.svg" alt="Coverage Status">




