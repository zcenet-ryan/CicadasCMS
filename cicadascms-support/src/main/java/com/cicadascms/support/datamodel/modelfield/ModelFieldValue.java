/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.datamodel.modelfield;

import com.cicadascms.support.datamodel.modelfield.value.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * BaseModelFieldType
 *
 * @author Jin
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = CheckboxValue.class, name = "checkbox"),
        @JsonSubTypes.Type(value = DateTimeValue.class, name = "dateTime"),
        @JsonSubTypes.Type(value = EditorValue.class, name = "editor"),
        @JsonSubTypes.Type(value = FileUploadValue.class, name = "fileUpload"),
        @JsonSubTypes.Type(value = ImgUploadValue.class, name = "imgUpload"),
        @JsonSubTypes.Type(value = InputValue.class, name = "input"),
        @JsonSubTypes.Type(value = MultiImgUploadValue.class, name = "multiImgUpload"),
        @JsonSubTypes.Type(value = SelectValue.class, name = "select"),
        @JsonSubTypes.Type(value = TextAreaValue.class, name = "textarea"),
        @JsonSubTypes.Type(value = RadioValue.class, name = "radio"),
        @JsonSubTypes.Type(value = JsonValue.class, name = "JSON")
})
public interface ModelFieldValue<T>{

    @JsonIgnore
    T getValue();

    String getType();



}
