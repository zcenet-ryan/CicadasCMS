package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.DictDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * DictInputDTO对象
 * 字典表
 * </p>
 *
 * @author jin
 * @since 2020-04-07
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="InputDictDTO对象")
public class DictInputDTO extends BaseDTO<DictInputDTO, DictDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 字典标识
    */
    @Schema(title = "1-字典标识" )
    private String dictCode;
    /**
    * 字典名称
    */
    @Schema(title = "2-字典名称" )
    private String dictName;
    /**
    * 字典值
    */
    @Schema(title = "3-字典值" )
    private String dictValue;
    /**
    * 上级ID
    */
    @Schema(title = "4-上级ID" )
    private Integer parentId;
    /**
    * 状态[0：不可用，1：可用]
    */
    @Schema(title = "5-状态[0：不可用，1：可用]" )
    private Boolean state;
    /**
    * 排序字段
    */
    @Schema(title = "6-排序字段" )
    private Integer sortId;
    /**
    * 是否删除-0：否，1：是
    */
    @Schema(title = "7-是否删除-0：否，1：是" )
    private Boolean isDelete;

    public static Converter<DictInputDTO, DictDO> converter = new Converter<DictInputDTO, DictDO>() {
        @Override
        public DictDO doForward(DictInputDTO dictInputDTO) {
            return WarpsUtils.copyTo(dictInputDTO, DictDO.class);
        }

        @Override
        public DictInputDTO doBackward(DictDO dict) {
            return WarpsUtils.copyTo(dict, DictInputDTO.class);
        }
    };

    @Override
    public DictDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public DictInputDTO convertFor(DictDO dict) {
        return converter.doBackward(dict);
    }
}
