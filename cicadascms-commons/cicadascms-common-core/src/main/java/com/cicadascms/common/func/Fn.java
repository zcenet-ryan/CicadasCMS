/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.func;


import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.cicadascms.common.utils.SpringContextUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;

import java.util.*;
import java.util.stream.Collectors;


/**
 * 常用函数
 *
 * @author Jin
 */
public class Fn {


    public static boolean contains(String obj1, String obj2) {
        return !Fn.isEmpty(obj1) && !Fn.isEmpty(obj2) && obj1.contains(obj2);
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return CollectionUtil.isNotEmpty(collection);
    }

    public static boolean isNotEmpty(Map<?, ?> map) {
        return MapUtil.isNotEmpty(map);
    }

    public static boolean isNotEmpty(String str) {
        return StrUtil.isNotEmpty(str);
    }

    public static boolean isNotNull(Object obj) {
        return ObjectUtil.isNotNull(obj);
    }

    public static boolean isNull(Object obj) {
        return ObjectUtil.isNull(obj);
    }

    public static boolean isEmpty(Collection<?> collection) {
        return CollectionUtil.isEmpty(collection);
    }

    public static boolean isEmpty(Map<?, ?> map) {
        return MapUtil.isEmpty(map);
    }

    public static boolean isEmpty(String str) {
        return StrUtil.isEmpty(str);
    }

    public static boolean equal(Object obj1, Object obj2) {
        return ObjectUtil.equal(obj1, obj2);
    }

    public static boolean notEqual(Object obj1, Object obj2) {
        return ObjectUtil.notEqual(obj1, obj2);
    }

    public static boolean startWithIgnoreCase(String str, String prefix) {
        return StrUtil.startWithIgnoreCase(str, prefix);
    }

    public static boolean startWith(String str, String prefix) {
        return StrUtil.startWith(str, prefix);
    }

    public static boolean endsWith(String str, String suffix) {
        return StrUtil.endWith(str, suffix);
    }

    public static boolean endsWithIgnoreCase(String str, String suffix) {
        return StrUtil.endWithIgnoreCase(str, suffix);
    }

    public static void copyProperties(Object source, Object target) {
        BeanUtils.copyProperties(source, target);
    }

    public static List<String> str2List(String str, String errorMsg) {
        if (isEmpty(str)) {
            throw new IllegalArgumentException(errorMsg);
        }
        if (!str.contains(",")) {
            return Collections.singletonList(str);
        }
        return Arrays.asList(str.split(","));
    }

    public static List<String> str2List(String str) {
        return str2List(str, "字符串转列表 - 参数错误");
    }

    public static String list2Str(List<Object> list) {
        if (isEmpty(list)) {
            throw new IllegalArgumentException("列表转字符串 - 参数错误");
        }
        return list.stream().map(Object::toString).collect(Collectors.joining(","));
    }

    public static String toJson(Object obj) {
        return new Gson().toJson(obj);
    }

    public static <T> T fromJson(String json, Class<T> type) {
        return new Gson().fromJson(json, type);
    }

    public static <T> List<T> fromJsonList(String json, Class<T> clazz) {
        List<T> list = new ArrayList<>();
        try {
            JsonArray array = JsonParser.parseString(json).getAsJsonArray();
            for (final JsonElement elem : array) {
                list.add(new Gson().fromJson(elem, clazz));
            }
        } catch (Exception e) {
            return null;
        }
        return list;
    }

    @SneakyThrows
    public static <T> T readValue(String json, Class<T> type) {
        ObjectMapper objectMapper = SpringContextUtils.getBean(ObjectMapper.class);
        return objectMapper.readValue(json, type);
    }

}
