package com.cicadascms.system.upms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.common.utils.SecurityUtils;
import com.cicadascms.security.LoginUserDetails;
import com.cicadascms.system.upms.service.IRoleService;
import com.cicadascms.system.upms.service.ITokenService;
import com.cicadascms.system.upms.vo.TokenVO;
import com.cicadascms.system.upms.vo.UserVO;
import com.cicadascms.system.upms.wrapper.UserWrapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Tag(name = "认证接口")
@Slf4j
@RestController
public class AuthController {

    @Autowired
    private ITokenService tokenService;
    @Autowired
    private IRoleService roleService;


    @Operation(summary = "密码模式获取令牌")
    @PostMapping(value = "/oauth/token")
    public OAuth2AccessToken passwordToken(
            @Parameter(name = HttpHeaders.AUTHORIZATION, description = "请求头", required = true)
            @RequestHeader(HttpHeaders.AUTHORIZATION) String header,
            @Parameter(name = "grant_type", description = "授权类型:password")
            @RequestParam(value = "grant_type", defaultValue = "password") String grantType,
            @Parameter(name = "username", description = "帐号", required = true)
            @RequestParam(value = "username") String username,
            @Parameter(name = "password", description = "密码", required = true)
            @RequestParam(value = "password") String password
    ) {
        return tokenService.passwordToken(header, grantType, username, password);
    }

    @Operation(summary = "手机号获取令牌")
    @PostMapping(value = "/oauth/mobile/token")
    public OAuth2AccessToken mobileToken(
            @Parameter(name = HttpHeaders.AUTHORIZATION, description = "请求头", required = true)
            @RequestHeader(HttpHeaders.AUTHORIZATION) String header,
            @Parameter(name = "grant_type", description = "授权类型:mobile")
            @RequestParam(value = "grant_type", defaultValue = "mobile") String grantType,
            @Parameter(name = "deviceId", description = "设备Id", required = true)
            @RequestParam(value = "deviceId") String deviceId,
            @Parameter(name = "mobile", description = "手机号", required = true)
            @RequestParam(value = "mobile") String mobile,
            @Parameter(name = "smsCode", description = "短信验证码", required = true)
            @RequestParam(value = "smsCode") String code
    ) {
        return tokenService.mobileToken(header, grantType, deviceId, mobile, code);
    }

    @Operation(summary = "微信小程序Code获取令牌")
    @PostMapping(value = "/oauth/wxma/token")
    public OAuth2AccessToken wxMaToken(
            @Parameter(name = HttpHeaders.AUTHORIZATION, description = "请求头", required = true)
            @RequestHeader(HttpHeaders.AUTHORIZATION) String header,
            @Parameter(name = "grant_type", description = "授权类型:wxma")
            @RequestParam(value = "grant_type", defaultValue = "wxma") String grantType,
            @Parameter(name = "js_code", description = "微信jsCode", required = true)
            @RequestParam(value = "js_code") String code
    ) {
        return tokenService.wxMaToken(header, grantType, code);
    }

    @Operation(summary = "微信授权码code获取令牌")
    @PostMapping(value = "/oauth/wxmp/token")
    public OAuth2AccessToken wxMpToken(
            @Parameter(name = HttpHeaders.AUTHORIZATION, description = "请求头", required = true)
            @RequestHeader(HttpHeaders.AUTHORIZATION) String header,
            @Parameter(name = "grant_type", description = "授权类型:wxmp")
            @RequestParam(value = "grant_type", defaultValue = "wxmp") String grantType,
            @Parameter(name = "code", description = "code", required = true)
            @RequestParam(value = "code") String code
    ) {
        return tokenService.wxMpToken(header, grantType, code);
    }

    @Operation(summary = "授权码模式获取令牌")
    @GetMapping(value = "/oauth/code/token")
    public OAuth2AccessToken authorizationCodeToken(
            @Parameter(name = "client_id", description = "应用id", required = true)
            @RequestParam(value = "client_id") String clientId,
            @Parameter(name = "client_secret", description = "应用密钥", required = true)
            @RequestParam(value = "client_secret") String clientSecret,
            @Parameter(name = "code", description = "授权码", required = true)
            @RequestParam(value = "code") String code,
            @Parameter(name = "grant_type", description = "授权类型:authorization_code")
            @RequestParam(value = "grant_type", defaultValue = "authorization_code") String grantType,
            @Parameter(name = "redirect_uri", description = "回调地址")
            @RequestParam(value = "redirect_uri") String redirectUri
    ) {
        return tokenService.authorizationCodeToken(clientId, clientSecret, code, grantType, redirectUri);
    }


    @Operation(summary = "客户端模式获取令牌")
    @PostMapping(value = "/oauth/client/token")
    public OAuth2AccessToken clientTokenInfo(@Parameter(name = HttpHeaders.AUTHORIZATION, description = "客户信息", required = true)
                                             @RequestHeader(HttpHeaders.AUTHORIZATION) String header) {

        return tokenService.clientToken(header);

    }

    @Operation(summary = "刷新令牌")
    @PostMapping(value = "/oauth/token/refresh")
    public OAuth2AccessToken refreshToken(
            @Parameter(name = "token", description = "令牌", required = true)
            @RequestParam(value = "token") String token) {

        return tokenService.refreshToken(token);
    }

    @Operation(summary = "获取令牌信息")
    @GetMapping(value = "/oauth/token/read")
    public OAuth2AccessToken getTokenInfo(
            @Parameter(name = "token", description = "令牌", required = true)
            @RequestParam(value = "token") String token) {

        return tokenService.getTokenInfo(token);

    }

    @Operation(summary = "获取用户信息")
    @PostMapping(value = "/oauth/userinfo")
    public R getUserInfo() {
        Map<String, Object> userInfo = new HashMap<>();
        LoginUserDetails loginUserDetails = (LoginUserDetails) SecurityUtils.getCurrentLoginUser();
        loginUserDetails.setPassword(null);
        UserVO userVO = UserWrapper.newBuilder().entityVO(loginUserDetails);
        userInfo.put("user", userVO);
        userInfo.put("roles", loginUserDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).filter(authority -> authority.startsWith("ROLE_")).collect(Collectors.toList()));
        userInfo.put("roleNames", loginUserDetails.getRoleIds().stream().map(rid -> roleService.getById(rid).getRoleName()).collect(Collectors.toList()));
        return R.ok(userInfo);
    }

    @Operation(summary = "令牌列表")
    @GetMapping(value = "/oauth/token/list")
    public R<Page<TokenVO>> tokenList(
            @Parameter(name = "页码", required = true)
            @RequestParam(value = "current", defaultValue = "1") Integer current,
            @Parameter(name = "数量", required = true)
            @RequestParam(value = "size", defaultValue = "10") Integer size) {
        return R.ok(tokenService.getTokenList(current, size));
    }

    @Operation(summary = "移除令牌")
    @PostMapping(value = "/oauth/token/remove")
    public R<String> removeToken(
            @Parameter(name = "令牌", required = true)
            @RequestParam(value = "token") String token) {
        return R.ok(tokenService.removeToken(token));
    }

    @Operation(summary = "登陆视图")
    @GetMapping("/account/signIn")
    public ModelAndView login() {
        return new ModelAndView("system/login");
    }

}
