/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.data.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.ChannelDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.io.Serializable;
import java.util.Map;

/**
 * <p>
 * 内容分类表 Mapper 接口
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface ChannelMapper extends BaseMapper<ChannelDO> {

    @Select("SELECT ${fields} FROM ${tableName} WHERE channel_id = #{channelId}")
    @ResultType(Map.class)
    Map<String, Object> selectByTableNameAndChannelId(@Param("fields") String fields, @Param("tableName") String tableName, @Param("channelId") Serializable channelId);

}
