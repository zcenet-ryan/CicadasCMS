/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.security;

import com.cicadascms.common.user.LoginUser;
import com.cicadascms.common.func.Fn;
import com.cicadascms.data.domain.UserDO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * LoginUserDetails
 *
 * @author Jin
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
public class LoginUserDetails extends UserDO implements LoginUser, UserDetails {

    private static final long serialVersionUID = 4125096758372084669L;


    @JsonIgnore
    private Set<String> roleKeys;

    @JsonIgnore
    private Set<Integer> roleIds;

    @JsonIgnore
    private Set<String> dataScopes;

    @JsonIgnore
    private Set<String> permissions;

    @JsonIgnore
    private Set<String> allowAccessUrls;

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> collection = new HashSet<>();
        if (Fn.isNotEmpty(roleKeys)) {
            roleKeys.parallelStream().forEach(roleKey -> collection.add(new SimpleGrantedAuthority("ROLE_" + roleKey)));
        }
        if (Fn.isNotEmpty(permissions)) {
            permissions.parallelStream().forEach(permission -> collection.add(new SimpleGrantedAuthority(permission)));
        }
        return collection;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Set<Integer> getRoleIds() {
        return roleIds;
    }

    @Override
    public Set<String> getDataScopes() {
        return dataScopes;
    }
}
