package com.cicadascms.website.admin.controller;

import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.resp.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "A-缓存管理接口")
@RestController
@RequestMapping("/admin/cms/cache")
@AllArgsConstructor
public class AdminCacheController {

    private List<BaseService> serviceList;

    @Autowired
    public void setServiceList(List<BaseService> serviceList) {
        this.serviceList = serviceList;
    }


    @Operation(summary = "缓存清理接口")
    @GetMapping("/clear")
    public R clear() {
        serviceList.parallelStream().forEach(BaseService::clearCache);
        return R.ok("缓存清理成功！",true);
    }

}
