package com.cicadascms.system.upms.vo;


import com.cicadascms.data.domain.DeptDO;
import com.cicadascms.data.domain.PositionDO;
import com.cicadascms.data.domain.RoleDO;
import com.cicadascms.data.domain.UserDO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

/**
 * @author westboy
 * @date 2019/6/8 12:47
 * @description: TODO
 */

@Data
@EqualsAndHashCode(callSuper = true)
@Schema(title = "UserVo", description = "登录用户")
public class UserVO extends UserDO {

    @JsonIgnore
    private Set<DeptDO> departments;

    @JsonIgnore
    private Set<RoleDO> roles;

    @JsonIgnore
    private Set<PositionDO> positions;

    @JsonIgnore
    private Set<String> permissions;

    @JsonIgnore
    private Set<String> allowAccessUrls;

    @Schema(title = "用户所属部门名称")
    public String deptName;
    @Schema(title = "用户所属部门编号")
    private Integer[] selectedDeptIds;

    @Schema(title = "用户所属职位名称")
    private String postName;
    @Schema(title = "用户所属职位编号")
    private Integer[] selectedPostIds;

    @Schema(title = "用户所属角色名称")
    private String roleName;
    @Schema(title = "用户所属角色编号")
    private Integer[] selectedRoleIds;


}
