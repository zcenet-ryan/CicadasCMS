package com.cicadascms.website.admin.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * ModelVO对象
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@Schema(title="ModelVO对象", description="内容模型表")
public class ModelVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer modelId;
    /**
    * 站点id
    */
    @Schema(title = "站点id" )
    private Long siteId;
    /**
    * 模型名称
    */
    @Schema(title = "模型名称" )
    private String modelName;
    /**
    * 模型表名称
    */
    @Schema(title = "模型表名称" )
    private String tableName;
    /**
    * 内容模型，栏目模型
    */
    @Schema(title = "内容模型，栏目模型" )
    private Integer modelType;
    /**
    * 字段描述
    */
    @Schema(title = "字段描述" )
    private String des;
    /**
    * 状态
    */
    @Schema(title = "状态" )
    private Boolean status;
    /**
    * 创建人
    */
    @Schema(title = "创建人" )
    private Integer createUser;
    /**
    * 创建时间
    */
    @Schema(title = "创建时间" )
    private LocalDateTime createTime;
    /**
    * 更新用户
    */
    @Schema(title = "-更新用户" )
    private Integer updateUser;
    /**
    * 更新时间
    */
    @Schema(title = "更新时间" )
    private LocalDateTime updateTime;
    /**
    * 删除状态
    */
    @Schema(title = "删除状态" )
    private Boolean deletedFlag;

}
