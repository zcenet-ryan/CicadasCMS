package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.RoleDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * SysRoleUpdateDTO对象
 * 角色表
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "RoleUpdateDTO对象")
public class RoleUpdateDTO extends BaseDTO<RoleUpdateDTO, RoleDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    @NotNull(message = "roleId不能为空")
        @Schema(title = "1-角色id")
    private Integer roleId;
    /**
     * 租户id
     */
        @Schema(title = "2-租户id")
    private Integer tenantId;
    /**
     * 父编号
     */
        @Schema(title = "3-父编号")
    private Integer parentId;
    /**
     * 角色名称
     */
    @NotEmpty
        @Schema(title = "4-角色名称")
    private String roleName;
    /**
     * 角色标识
     */
    @NotEmpty
        @Schema(title = "5-角色标识")
    private String roleKey;
    /**
     * 菜单类型(1，系统角色，2，应用角色)
     */
        @Schema(title = "6-菜单类型(1，系统角色，2，应用角色)")
    private Integer roleType;
    /**
     * 说明
     */
        @Schema(title = "7-说明")
    private String remark;

    /**
     * 权限编号
     */
        @Schema(title = "8-权限编号")
    @NotNull
    private String permissionIds;

    public List<String> getPermissionIds() {
        if (Fn.isEmpty(permissionIds)) {
            return null;
        }
        return Fn.str2List(permissionIds);
    }

    public static Converter<RoleUpdateDTO, RoleDO> converter = new Converter<RoleUpdateDTO, RoleDO>() {
        @Override
        public RoleDO doForward(RoleUpdateDTO roleUpdateDTO) {
            return WarpsUtils.copyTo(roleUpdateDTO, RoleDO.class);
        }

        @Override
        public RoleUpdateDTO doBackward(RoleDO role) {
            return WarpsUtils.copyTo(role, RoleUpdateDTO.class);
        }
    };

    @Override
    public RoleDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public RoleUpdateDTO convertFor(RoleDO role) {
        return converter.doBackward(role);
    }
}
