package java.cn.inhct.core.swagger.properties;


import com.cicadascms.common.func.Fn;
import io.swagger.v3.oas.models.security.Scopes;
import io.swagger.v3.oas.models.servers.Server;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * SwaggerConfig
 *
 * @author Jin
 */
@Data
@ConfigurationProperties("swagger")
public class SwaggerProperties {

    private String title = "";

    private String description = "";

    private Server server = new Server();

    private String version = "";

    private String license = "";

    private String licenseUrl = "";

    private String termsOfServiceUrl = "";

    private String host = "";

    private Contact contact = new Contact();

    private OAuth2 OAuth2 = new OAuth2();

    @Data
    @NoArgsConstructor
    public static class Contact {

        private String name = "";
        private String url = "";
        private String email = "";

    }

    @Data
    @NoArgsConstructor
    public static class OAuth2 {
        private String tokenUrl = "";
        private List<Scope> scopeList = new ArrayList<>();

        public Scopes getScopes() {
            Scopes scopes = new Scopes();
            if (Fn.isEmpty(scopeList)) {
                scopeList.stream().parallel().forEach(e -> scopes.addString(e.getDescription(), e.getScope()));
            }
            return scopes;
        }

    }

    @Data
    @NoArgsConstructor
    public static class Scope {
        private String scope = "";
        private String description = "";
    }
}
