package com.cicadascms.website.front.controller.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cicadascms.common.exception.FrontNotFoundException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.website.front.service.IChannelService;
import com.cicadascms.website.front.service.IContentService;
import com.cicadascms.website.front.vo.ChannelVO;
import com.cicadascms.website.front.vo.ContentVO;
import com.cicadascms.support.annotation.CurrentWebSite;
import com.cicadascms.website.util.WebSiteViewUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ChannelController {
    private static final int DEFAULT_PAGE_NUMBER = 1;
    private IChannelService channelService;
    private IContentService contentService;

    @RequestMapping("/{channelUrlPath}")
    public String index(@PathVariable String channelUrlPath, @CurrentWebSite SiteDO siteDO, Model model) {
        return page(channelUrlPath, DEFAULT_PAGE_NUMBER, siteDO, model);
    }


    @RequestMapping("/{channelUrlPath}_{pageNumber:\\d+}")
    public String page(@PathVariable String channelUrlPath, @PathVariable Integer pageNumber, @CurrentWebSite SiteDO siteDO, Model model) {
        ChannelVO channelVO = channelService.findBySiteIdAndChannelUrlPath(siteDO.getSiteId(), channelUrlPath);
        if (Fn.isNull(channelVO)) {
            throw new FrontNotFoundException("栏目已被删除或不存在！");
        }
        model.addAttribute("channel", channelVO);
        IPage<ContentVO> page = contentService.findByPageNumberAndChannelId(pageNumber, channelVO.getChannelId());
        model.addAttribute("page", page);
        model.addAttribute("site", siteDO);
        return WebSiteViewUtil.channelViewRender(siteDO.getPcTemplateDir(), siteDO.getMobileTemplateDir(), channelVO.getChannelView());
    }

    @Autowired
    public void setContentService(IContentService contentService) {
        this.contentService = contentService;
    }

    @Autowired
    public void setChannelService(IChannelService channelService) {
        this.channelService = channelService;
    }


}
