package com.cicadascms.system.upms.dto;


import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.DeptDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * DeptInputDTO对象
 * 机构部门
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="InputDeptDTO对象")
public class DeptInputDTO extends BaseDTO<DeptInputDTO, DeptDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 部门名称
    */
    @Schema(title = "1-部门名称" )
    private String deptName;
    /**
    * 部门编码
    */
    @Schema(title = "2-部门编码" )
    private String deptCode;
    /**
    * 父id
    */
    @Schema(title = "3-父id" )
    private Integer parentId;
    /**
    * 排序字段
    */
    @Schema(title = "4-排序字段" )
    private Integer sortId;
    /**
    * 备注
    */
    @Schema(title = "5-备注" )
    private String remark;
    /**
    * 租户id
    */
    @Schema(title = "6-租户id" )
    private Integer tenantId;

    public static Converter<DeptInputDTO, DeptDO> converter = new Converter<DeptInputDTO, DeptDO>() {
        @Override
        public DeptDO doForward(DeptInputDTO deptInputDTO) {
            return WarpsUtils.copyTo(deptInputDTO, DeptDO.class);
        }

        @Override
        public DeptInputDTO doBackward(DeptDO dept) {
            return WarpsUtils.copyTo(dept, DeptInputDTO.class);
        }
    };

    @Override
    public DeptDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public DeptInputDTO convertFor(DeptDO dept) {
        return converter.doBackward(dept);
    }
}
