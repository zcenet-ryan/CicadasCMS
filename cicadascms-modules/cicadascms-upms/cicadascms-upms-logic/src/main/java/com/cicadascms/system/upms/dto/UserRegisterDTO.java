package com.cicadascms.system.upms.dto;


import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.UserDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * UserInputDTO对象
 * 基础账户表
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="InputUserDTO对象")
public class UserRegisterDTO extends BaseDTO<UserRegisterDTO, UserDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 邮箱
    */
        @Schema(title = "邮箱")
    private String email;
    /**
    * 手机号
    */
        @Schema(title = "手机号")
    private String phone;
    /**
    * 登陆名
    */
        @Schema(title = "账号名")
    private String username;
    /**
    * 密码
    */
        @Schema(title = "密码")
    private String password;

        @Schema(title = "真实姓名" )
    private String realName;

        @Schema(title = "头像" )
    private String avatar;


    public static Converter<UserRegisterDTO, UserDO> converter = new Converter<UserRegisterDTO, UserDO>() {
        @Override
        public UserDO doForward(UserRegisterDTO userInputDTO) {
            return WarpsUtils.copyTo(userInputDTO, UserDO.class);
        }

        @Override
        public UserRegisterDTO doBackward(UserDO user) {
            return WarpsUtils.copyTo(user, UserRegisterDTO.class);
        }
    };

    @Override
    public UserDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public UserRegisterDTO convertFor(UserDO user) {
        return converter.doBackward(user);
    }
}
