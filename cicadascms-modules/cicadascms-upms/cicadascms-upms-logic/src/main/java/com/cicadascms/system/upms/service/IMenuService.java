package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.MenuDO;
import com.cicadascms.system.upms.dto.MenuInputDTO;
import com.cicadascms.system.upms.dto.MenuQueryDTO;
import com.cicadascms.system.upms.dto.MenuUpdateDTO;
import com.cicadascms.system.upms.vo.MenuVO;
import com.cicadascms.system.upms.vo.RouteVO;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author westboy
 * @date 2019-07-10
 */
public interface IMenuService extends IService<MenuDO> {

    List<MenuVO> findList(MenuQueryDTO menuQueryDTO);

    List<MenuVO> getTree();

    List<MenuDO> findByParentId(Integer id);

    List<RouteVO> findRoutes();

    List<MenuDO> findByRoleIds(Set<Integer> RoleIds);

    List<MenuDO> findByRoleId(Integer roleId);

    /**
     * 异步保存方法
     *
     * @param menuInputDTO
     * @return
     */
    void asyncSave(MenuInputDTO menuInputDTO);

    /**
     * 异步更新方法
     *
     * @param menuUpdateDTO
     * @return
     */
    void asyncUpdate(MenuUpdateDTO menuUpdateDTO);

    /**
     * 保存方法
     *
     * @param menuInputDTO
     * @return
     */
    R<Boolean> save(MenuInputDTO menuInputDTO);

    /**
     * 更新方法
     *
     * @param menuUpdateDTO
     * @return
     */
    R<Boolean> update(MenuUpdateDTO menuUpdateDTO);

    /**
     * 查询方法
     *
     * @param id
     * @return
     */
    R<MenuVO>  findById(Serializable id);

    /**
     * 删除方法
     *
     * @param id
     * @return
     */
    R<Boolean>  deleteById(Serializable id);

}
