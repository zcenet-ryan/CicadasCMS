package com.cicadascms.website.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.website.admin.dto.ChannelInputDTO;
import com.cicadascms.website.admin.dto.ChannelQueryDTO;
import com.cicadascms.website.admin.dto.ChannelUpdateDTO;
import com.cicadascms.website.admin.service.IAdminChannelService;
import com.cicadascms.website.admin.vo.ChannelVO;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.support.annotation.CurrentWebSite;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * <p>
 * 栏目 控制器
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Api(tags = "A-栏目接口")
@RestController
@RequestMapping("/admin/cms/channel")
@AllArgsConstructor
public class AdminChannelController {
    private final IAdminChannelService channelService;

    @Operation(summary = "栏目树接口")
    @GetMapping("/tree")
    public R<ChannelVO> getTree(@CurrentWebSite SiteDO siteDO) {
        if (Fn.isNull(siteDO)) R.error("站点不存在！", false);
        return R.ok(channelService.getTree(siteDO.getSiteId()));
    }

    @Operation(summary = "栏目分页接口")
    @GetMapping("/list")
    public R<Page<ChannelVO>> page(ChannelQueryDTO channelQueryDTO) {
        return channelService.page(channelQueryDTO);
    }

    @Operation(summary = "栏目保存接口")
    @PostMapping
    public R<Boolean> save(@Valid @RequestBody ChannelInputDTO channelInputDTO) {
        return channelService.save(channelInputDTO);
    }

    @Operation(summary = "栏目更新接口")
    @PutMapping
    public R<Boolean> updateById(@Valid @RequestBody ChannelUpdateDTO channelUpdateDTO) {
        return channelService.update(channelUpdateDTO);
    }

    @Operation(summary = "栏目详情接口")
    @GetMapping("/{id}")
    public R<ChannelVO> getById(@PathVariable Integer id) {
        return channelService.findById(id);
    }

    @Operation(summary = "栏目删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Integer id) {
        return channelService.deleteById(id);
    }


}
