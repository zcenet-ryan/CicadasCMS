package com.cicadascms.system.upms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.LogDO;
import com.cicadascms.system.upms.dto.LogQueryDTO;
import com.cicadascms.system.upms.service.ILogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 * 日志表 控制器
 * </p>
 *
 * @author jin
 * @since 2020-05-05
 */
@Tag(name = "日志管理接口")
@RestController
@RequestMapping("/system/log")
@AllArgsConstructor
public class LogController {
    private final ILogService logService;

    @Operation(summary = "日志列表接口")
    @GetMapping("/page")
    public R<Page<LogDO>> getPage(LogQueryDTO logQueryDTO) {
        return logService.page(logQueryDTO);
    }

    @Operation(summary = "日志详情接口")
    @GetMapping("/{id}")
    public R<LogDO> getById(@PathVariable Long id) {
        return logService.findById(id);
    }

    @Operation(summary = "日志删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return logService.deleteById(id);
    }


}
