/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.support.captch;


import com.cicadascms.common.resp.R;

/**
 * ValidateCodeService
 *
 * @author Jin
 */
public interface ValidateCodeService {


    /**
     * 创建验证码
     */
    R create();


    /**
     * 图形验证码校验
     *
     * @param deviceId
     * @param inputCodeValue
     */
    void verifyCaptcha(String deviceId, String inputCodeValue);

    /**
     * 短信验证码校验
     *
     * @param deviceId
     * @param mobile
     * @param inputCodeValue
     */
    void verifySmsCode(String deviceId, String mobile, String inputCodeValue);

}
