/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.dict;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Dict
 *
 * @author Jin
 */
@Getter
@Setter
public class Dict {

    private Integer id;

    /**
     * 字典标识
     */
    private String dictCode;

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典值
     */
    private Integer dictValue;

    /**
     * 上级ID
     */
    private Integer parentId;

    /**
     * 状态[0：不可用，1：可用]
     */
    private Boolean state;

    /**
     * 排序字段
     */
    private Integer sortId;

    /**
     * 排序字段
     */
    private String remarks;

    /**
     * parentName
     */
    private String parentName;


    /**
     * children
     */
    private List<Dict> children;

}
