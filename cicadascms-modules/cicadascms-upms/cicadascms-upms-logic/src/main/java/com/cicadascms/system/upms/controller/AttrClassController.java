package com.cicadascms.system.upms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.system.upms.dto.AttrClassInputDTO;
import com.cicadascms.system.upms.dto.AttrClassQueryDTO;
import com.cicadascms.system.upms.dto.AttrClassUpdateDTO;
import com.cicadascms.system.upms.service.IAttrClassService;
import com.cicadascms.system.upms.vo.AttrClassVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * <p>
 * 附件分类 控制器
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Tag(name = "附件分类接口")
@RestController
@RequestMapping("/system/attrClass")
@AllArgsConstructor
public class AttrClassController {
    private final IAttrClassService attrClassService;

    @Operation(summary = "附件分类分页接口")
    @GetMapping("/list")
    public R<Page<AttrClassVO>> page(AttrClassQueryDTO attrClassQueryDTO) {
        return attrClassService.page(attrClassQueryDTO);
    }

    @Operation(summary = "附件分类保存接口")
    @PostMapping
    public R<Boolean> save(@Valid AttrClassInputDTO attrClassInputDTO) {
        return attrClassService.save(attrClassInputDTO);
    }

    @Operation(summary = "附件分类更新接口")
    @PutMapping
    public R<Boolean> updateById(@Valid AttrClassUpdateDTO attrClassUpdateDTO) {
        return attrClassService.update(attrClassUpdateDTO);
    }

    @Operation(summary = "附件分类详情接口")
    @GetMapping("/{id}")
    public R<AttrClassVO> getById(@PathVariable Long id) {
        return attrClassService.findById(id);
    }

    @Operation(summary = "附件分类删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return attrClassService.deleteById(id);
    }


}
