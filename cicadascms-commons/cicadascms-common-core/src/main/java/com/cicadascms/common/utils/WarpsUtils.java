/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ReflectUtil;
import lombok.experimental.UtilityClass;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * SpringContextUtils
 *
 * @author Jin
 */
@UtilityClass
public class WarpsUtils {

    public static <T> T copyTo(Object object, Class<T> clazz) {
        try {
            T vo = ReflectUtil.newInstance(clazz);
            BeanUtil.copyProperties(object, vo);
            return vo;
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> T convertTo(Object bean, Class<T> clazz) {
        try {
            ObjectMapper mapper = SpringContextUtils.getBean(ObjectMapper.class);
            return mapper.convertValue(bean, clazz);
        } catch (Exception e) {
            return null;
        }
    }
}
