/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.launcher.filter;

import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.security.ClientsUtils;
import com.cicadascms.support.captch.ValidateCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * GlobalVerifyFilter
 *
 * @author Jin
 */
@Component
public class VerifyFilter extends GenericFilterBean {
    private ValidateCodeService imageValidateCodeService;
    private AuthenticationFailureHandler restLoginFailureHandler;

    private final AntPathMatcher pathMatcher = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        String deviceId = httpServletRequest.getParameter(Constant.VALIDATE_PARAMETER_NAME_DEVICE_ID);
        String inputCodeValue = httpServletRequest.getParameter(Constant.VALIDATE_PARAMETER_NAME_CODE);

        try {
            if (pathMatcher.match("/oauth/token", httpServletRequest.getRequestURI()) && Fn.equal("POST", httpServletRequest.getMethod())) {
                String header = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
                String referer = httpServletRequest.getHeader("Referer");
                if (!Fn.isEmpty(header)) {
                    ClientsUtils.ClientInfo client = ClientsUtils.buildClientInfo(header);
                    //放行不做验证码校验
                    if (Fn.notEqual(Constant.VALIDATE_TEST_CLIENT, client.getClientId())) {
                        imageValidateCodeService.verifyCaptcha(deviceId, inputCodeValue);
                    }
                } else {
                    throw new ServiceException("无法获取clientId！");
                }
            }
        } catch (ServiceException serviceException) {
            restLoginFailureHandler.onAuthenticationFailure(httpServletRequest, httpServletResponse, new InsufficientAuthenticationException("验证码校验未通过！", serviceException));
            return;
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    @Autowired
    public void setImageValidateCodeService(ValidateCodeService imageValidateCodeService) {
        this.imageValidateCodeService = imageValidateCodeService;
    }

    @Autowired
    public void setRestLoginFailureHandler(AuthenticationFailureHandler restLoginFailureHandler) {
        this.restLoginFailureHandler = restLoginFailureHandler;
    }
}
