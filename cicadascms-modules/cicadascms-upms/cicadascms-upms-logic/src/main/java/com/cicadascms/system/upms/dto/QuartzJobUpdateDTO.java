package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.QuartzJobDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * SysQuartzJobUpdateDTO对象
 * 定时任务
 * </p>
 *
 * @author jin
 * @since 2020-04-29
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="QuartzJobUpdateDTO对象")
public class QuartzJobUpdateDTO extends BaseDTO<QuartzJobUpdateDTO, QuartzJobDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 自增主键
    */
    @Schema(title = "1-自增主键" )
    private Integer id;
    /**
    * 任务名称
    */
    @Schema(title = "2-任务名称" )
    private String jobName;
    /**
    * 任务分组
    */
    @Schema(title = "3-任务分组" )
    private String jobGroup;
    /**
    * 执行类
    */
    @Schema(title = "4-执行类" )
    private String jobClassName;
    /**
    * cron表达式
    */
    @Schema(title = "5-cron表达式" )
    private String cronExpression;
    /**
    * 任务状态
    */
    @Schema(title = "6-任务状态" )
    private String triggerState;
    /**
    * 修改之前的任务名称
    */
    @Schema(title = "7-修改之前的任务名称" )
    private String oldJobName;
    /**
    * 修改之前的任务分组
    */
    @Schema(title = "8-修改之前的任务分组" )
    private String oldJobGroup;
    /**
    * 描述
    */
    @Schema(title = "9-描述" )
    private String description;

    public static Converter<QuartzJobUpdateDTO, QuartzJobDO> converter = new Converter<QuartzJobUpdateDTO, QuartzJobDO>() {
        @Override
        public QuartzJobDO doForward(QuartzJobUpdateDTO quartzJobUpdateDTO) {
            return WarpsUtils.copyTo(quartzJobUpdateDTO, QuartzJobDO.class);
        }

        @Override
        public QuartzJobUpdateDTO doBackward(QuartzJobDO quartzJob) {
            return WarpsUtils.copyTo(quartzJob, QuartzJobUpdateDTO.class);
        }
    };

    @Override
    public QuartzJobDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public QuartzJobUpdateDTO convertFor(QuartzJobDO quartzJob) {
        return converter.doBackward(quartzJob);
    }
}
