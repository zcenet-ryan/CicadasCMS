/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.logger.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * LogTypeEnum
 *
 * @author Jin
 */
@Getter
@AllArgsConstructor
public enum LogTypeEnum {

    操作日志("OPERA", "操作日志"),
    错误日志("ERROR", "错误日志");

    private final String type;
    private final String description;
}
