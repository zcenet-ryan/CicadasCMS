package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.PositionDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * PositionInputDTO对象
 * 职位表
 * </p>
 *
 * @author jin
 * @since 2020-08-25
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="InputPositionDTO对象")
public class PositionInputDTO extends BaseDTO<PositionInputDTO, PositionDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 职位名称
    */
    @Schema(title = "1-职位名称" )
    private String postName;
    /**
    * 职位编号
    */
    @Schema(title = "2-职位编号" )
    private String postCode;
    /**
    * 职位类型字典表(post_type)
    */
    @Schema(title = "3-职位类型字典表(post_type)" )
    private Integer postType;
    /**
    * 排序字段
    */
    @Schema(title = "4-排序字段" )
    private Integer sortId;


    public static Converter<PositionInputDTO, PositionDO> converter = new Converter<PositionInputDTO, PositionDO>() {
        @Override
        public PositionDO doForward(PositionInputDTO positionInputDTO) {
            return WarpsUtils.copyTo(positionInputDTO, PositionDO.class);
        }

        @Override
        public PositionInputDTO doBackward(PositionDO position) {
            return WarpsUtils.copyTo(position, PositionInputDTO.class);
        }
    };

    @Override
    public PositionDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public PositionInputDTO convertFor(PositionDO position) {
        return converter.doBackward(position);
    }
}
