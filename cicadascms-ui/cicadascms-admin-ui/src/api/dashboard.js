
export function clearCache() {
  return request({
    url: '/admin/cache/clear/all',
    method: 'get'
  })
}
