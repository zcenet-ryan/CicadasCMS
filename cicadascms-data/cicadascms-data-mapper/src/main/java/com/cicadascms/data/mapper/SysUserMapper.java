/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.data.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cicadascms.data.domain.UserDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 基础账户表 Mapper 接口
 * </p>
 *
 * @author westboy
 * @date 2019-05-23
 */
public interface SysUserMapper extends BaseMapper<UserDO> {

    <E extends IPage<UserDO>> E selectUserPage(E page,
                                               @Param("deptIds") List<Integer> deptIds,
                                               @Param("username") String username,
                                               @Param("userType") Integer userType,
                                               @Param("status") Integer status);


    <E extends IPage<UserDO>> E selectUserPageByDeptCode(E page,
                                                         @Param("deptCodes") List<String> deptCodes,
                                                         @Param("username") String username,
                                                         @Param("userType") Integer userType,
                                                         @Param("status") Integer status);

}
