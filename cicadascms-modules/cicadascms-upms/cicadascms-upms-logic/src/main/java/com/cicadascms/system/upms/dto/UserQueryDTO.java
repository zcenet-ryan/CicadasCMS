package com.cicadascms.system.upms.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.UserDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * UserQueryDTO对象
 * 基础账户表
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "UserQueryDTO对象")
public class UserQueryDTO extends BaseDTO<UserQueryDTO, UserDO> implements Serializable {

    private static final long serialVersionUID = 1L;

        @Schema(title = "1-当前页码")
    public long current = Constant.PAGE_NUM;

        @Schema(title = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

        @Schema(title = "降序排序字段 多个字段用英文逗号隔开")
    private String descs;

        @Schema(title = "升序排序字段  多个字段用英文逗号隔开")
    private String ascs;

    /**
     * 邮箱
     */
        @Schema(title = "邮箱")
    private String email;
    /**
     * 手机号
     */
        @Schema(title = "手机号")
    private String phone;
    /**
     * 登陆名
     */
        @Schema(title = "登陆名")
    private String username;

    /**
     * 用户类型（1.系统管理员；2,入驻药店；3.平台管理员；4.平台会员，5.入驻医生）
     */
        @Schema(title = "用户类型（1.系统管理员；2,入驻药店；3.平台管理员；4.平台会员，5.入驻医生）")
    private Integer userType;

    /**
     * 状态 1:enable, 0:disable, -1:deleted
     */
        @Schema(title = "状态 1:enable, 0:disable, -1:deleted")
    private Integer status;

        @Schema(title = "部门编号")
    private Integer deptId;


    public Page<UserDO> page() {
        Page<UserDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<UserQueryDTO, UserDO> converter = new Converter<UserQueryDTO, UserDO>() {
        @Override
        public UserDO doForward(UserQueryDTO userQueryDTO) {
            return WarpsUtils.copyTo(userQueryDTO, UserDO.class);
        }

        @Override
        public UserQueryDTO doBackward(UserDO user) {
            return WarpsUtils.copyTo(user, UserQueryDTO.class);
        }
    };

    @Override
    public UserDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public UserQueryDTO convertFor(UserDO user) {
        return converter.doBackward(user);
    }
}
