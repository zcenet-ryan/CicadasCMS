package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.AreaDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * SysAreaUpdateDTO对象
 * 区域
 * </p>
 *
 * @author jin
 * @since 2020-09-06
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="AreaUpdateDTO对象")
public class AreaUpdateDTO extends BaseDTO<AreaUpdateDTO, AreaDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
    * 名称
    */
    @Schema(title = "2-名称" )
    private String name;
    /**
    * 父编号
    */
    @Schema(title = "3-父编号" )
    private String parentId;
    /**
    * 简称
    */
    @Schema(title = "4-简称" )
    private String shortName;
    /**
    * 级别
    */
    @Schema(title = "5-级别" )
    private Integer levelType;
    /**
    * 城市代码
    */
    @Schema(title = "6-城市代码" )
    private String ctyCode;
    /**
    * 邮编
    */
    @Schema(title = "7-邮编" )
    private String zipCode;
    /**
    * 详细名称
    */
    @Schema(title = "8-详细名称" )
    private String mergerName;
    /**
    * 经度
    */
    @Schema(title = "9-经度" )
    private String lng;
    /**
    * 维度
    */
    @Schema(title = "10-维度" )
    private String lat;
    /**
    * 拼音
    */
    @Schema(title = "11-拼音" )
    private String pinyin;

    public static Converter<AreaUpdateDTO, AreaDO> converter = new Converter<AreaUpdateDTO, AreaDO>() {
        @Override
        public AreaDO doForward(AreaUpdateDTO areaUpdateDTO) {
            return WarpsUtils.copyTo(areaUpdateDTO, AreaDO.class);
        }

        @Override
        public AreaUpdateDTO doBackward(AreaDO area) {
            return WarpsUtils.copyTo(area, AreaUpdateDTO.class);
        }
    };

    @Override
    public AreaDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public AreaUpdateDTO convertFor(AreaDO area) {
        return converter.doBackward(area);
    }
}
