package com.cicadascms.website.admin.service;




import com.cicadascms.website.admin.dto.TemplateFileInputDTO;
import com.cicadascms.website.admin.dto.TemplateFileUpdateDTO;
import com.cicadascms.website.admin.vo.TemplateFileVO;

import java.util.List;

/**
 * <p>
 * 模管理 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface IAdminTemplateService {


    boolean saveTemplateFile(TemplateFileInputDTO templateFileInputDTO);

    boolean updateTemplateFile(TemplateFileUpdateDTO templateFileUpdateDTO);

    boolean deleteTemplateFile(String filePath);

    List<TemplateFileVO> getTemplateFileList(String filePath);

    TemplateFileVO find(String fileName);

}
