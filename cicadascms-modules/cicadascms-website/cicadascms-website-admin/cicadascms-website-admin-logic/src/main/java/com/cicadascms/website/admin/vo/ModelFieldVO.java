package com.cicadascms.website.admin.vo;

import com.cicadascms.support.datamodel.modelfield.ModelFieldProp;
import com.cicadascms.support.datamodel.modelfield.ModelFieldRule;
import com.cicadascms.support.datamodel.modelfield.ModelFieldValue;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * ModelFieldVO对象
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@Schema(title = "ModelFieldVO", description = "模型字段")
public class ModelFieldVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Schema(title = "主键")
    private Integer fieldId;
    /**
     * 模型字段
     */
    @Schema(title = "模型字段")
    private Integer modelId;
    /**
     * 字段名称
     */
    @Schema(title = "字段名称")
    private String fieldName;
    /**
     * 字段类型
     */
    @Schema(title = "字段类型")
    private Integer fieldType;
    /**
     * 字段配置
     */
    @Schema(title = "字段配置")
    private ModelFieldProp<ModelFieldRule, ModelFieldValue<?>> fieldProp;
    /**
     * 是否检索字段
     */
    @Schema(title = "是否检索字段")
    private Boolean isSearchField;
    /**
     * 字段描述
     */
    @Schema(title = "字段描述")
    private String des;
    /**
     * 更新用户
     */
    @Schema(title = "更新用户")
    private Integer updateUser;
    /**
     * 更新时间
     */
    @Schema(title = "更新时间")
    private LocalDateTime updateTime;
    /**
     * 创建用户
     */
    @Schema(title = "创建用户")
    private Integer createUser;
    /**
     * 创建时间
     */
    @Schema(title = "创建时间")
    private LocalDateTime createTime;


}
