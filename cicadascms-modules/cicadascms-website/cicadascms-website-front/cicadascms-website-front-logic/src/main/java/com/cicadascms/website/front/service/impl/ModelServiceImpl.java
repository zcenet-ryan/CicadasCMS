package com.cicadascms.website.front.service.impl;

import com.cicadascms.common.base.BaseService;
import com.cicadascms.data.domain.ModelDO;
import com.cicadascms.data.mapper.ModelMapper;
import com.cicadascms.website.front.service.IModelService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 内容模型表 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Service("modelService")
@AllArgsConstructor
public class ModelServiceImpl extends BaseService<ModelMapper, ModelDO> implements IModelService {


    @Override
    protected String getCacheName() {
        return "dataModelCache";
    }

    @Override
    public ModelDO getModelById(Integer modelId) {
        return getCacheForEntity("modelId" + ":" + modelId, () -> getById(modelId));
    }
}
