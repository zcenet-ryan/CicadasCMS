package com.cicadascms.website.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.website.admin.dto.ModelInputDTO;
import com.cicadascms.website.admin.dto.ModelQueryDTO;
import com.cicadascms.website.admin.dto.ModelUpdateDTO;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.DbUtils;
import com.cicadascms.data.domain.ModelDO;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.data.mapper.ModelFieldMapper;
import com.cicadascms.data.mapper.ModelMapper;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.resp.R;
import com.cicadascms.website.admin.service.IAdminModelService;
import com.cicadascms.support.datamodel.DataModelSqlBuilder;
import com.cicadascms.support.datamodel.constant.MysqlColumnTypeEnum;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 内容模型表 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Service
@AllArgsConstructor
public class AdminModelServiceImpl extends BaseService<ModelMapper, ModelDO> implements IAdminModelService {
    private final DataSource dataSource;
    private final ModelFieldMapper modelFieldMapper;

    public static final Integer DEFAULT_CHANNEL_TYPE_CODE = 1;
    public static final String DEFAULT_CHANNEL_ID = "channel_id";
    public static final String DEFAULT_CHANNEL_TABLE_PREFIX = "cms_channel_";

    public static final Integer DEFAULT_CONTENT_TYPE_CODE = 2;
    public static final String DEFAULT_CONTENT_ID = "content_id";
    public static final String DEFAULT_CONTENT_TABLE_PREFIX = "cms_content_";

    public static final Integer DEFAULT_FORM_TYPE_CODE = 3;
    public static final String DEFAULT_FORM_ID = "form_id";
    public static final String DEFAULT_FORM_TABLE_PREFIX = "cms_form_";


    @Override
    public R page(ModelQueryDTO modelQueryDTO) {
        ModelDO modelDO = modelQueryDTO.convertToEntity();
        Page page = baseMapper.selectPage(modelQueryDTO.page(), getLambdaQueryWrapper().setEntity(modelDO));
        return R.ok(page);
    }

    @Transactional
    @Override
    public R save(ModelInputDTO modelInputDTO) {
        try {
            ModelDO modelDO = modelInputDTO.convertToEntity();
            String sql = "";
            //Jin 栏目模型
            if (Fn.equal(modelDO.getModelType(), DEFAULT_CHANNEL_TYPE_CODE)) {
                String tableName = DEFAULT_CHANNEL_TABLE_PREFIX.concat(modelDO.getTableName());
                modelDO.setTableName(tableName);
                sql = getCreateTableSql(tableName, DEFAULT_CHANNEL_ID);
            }
            //Jin 内容模型
            if (Fn.equal(modelDO.getModelType(), DEFAULT_CONTENT_TYPE_CODE)) {
                String tableName = DEFAULT_CONTENT_TABLE_PREFIX.concat(modelDO.getTableName());
                modelDO.setTableName(tableName);
                sql = getCreateTableSql(tableName, DEFAULT_CONTENT_ID);
            }
            //Jin 表单模型
            if (Fn.equal(modelDO.getModelType(), DEFAULT_FORM_TYPE_CODE)) {
                String tableName = DEFAULT_FORM_TABLE_PREFIX.concat(modelDO.getTableName());
                modelDO.setTableName(tableName);
                sql = getCreateTableSql(tableName, DEFAULT_FORM_ID);
            }
            save(modelDO);
            DbUtils.use(dataSource).exec(sql);
        } catch (Exception e) {
            throw new ServiceException("新增模型出错！", e);
        }
        return R.ok(true);
    }

    private String getCreateTableSql(String tableName, String primaryKey) {
        return DataModelSqlBuilder.newBuilder().newCreateTableSqlBuilder()
                .tableName(tableName)
                .initColumn(primaryKey, MysqlColumnTypeEnum.INT.getCode(), 11, false, null, true, true)
                .buildSql();
    }

    @Override
    public R update(ModelUpdateDTO modelUpdateDTO) {
        try {
            ModelDO newModelDO = modelUpdateDTO.convertToEntity();
            ModelDO oldModelDO = getById(newModelDO.getModelId());
            //Jin 阔赞表名修改
            if (updateById(newModelDO) && Fn.notEqual(newModelDO.getTableName(), oldModelDO.getTableName())) {
                String sql = DataModelSqlBuilder
                        .newBuilder()
                        .newCreateTableSqlBuilder()
                        .tableName(oldModelDO.getTableName())
                        .rename(newModelDO.getTableName())
                        .buildSql();
                DbUtils.use(dataSource).exec(sql);
            }
        } catch (Exception e) {
            throw new ServiceException("新增模型出错！", e);
        }

        return R.ok(true);
    }

    @Override
    public R findById(Serializable id) {
        ModelDO modelDO = baseMapper.selectById(id);
        return R.ok(modelDO);

    }

    @Override
    public R findByType(Serializable type) {
        List<ModelDO> modelFieldDOS =  list(getLambdaQueryWrapper().eq(ModelDO::getModelType,type));
        return R.ok(modelFieldDOS);
    }


    @Transactional
    @Override
    public R deleteById(Serializable id) {
        ModelDO modelDO = getById(id);
        if (Fn.isNull(modelDO)) throw new ServiceException("模型不存在！");
        //Jin 先删除扩展字段
        modelFieldMapper.delete(new LambdaQueryWrapper<ModelFieldDO>().eq(ModelFieldDO::getModelId, modelDO.getModelId()));
        //Jin 再删除扩展表
        String sql = DataModelSqlBuilder.newBuilder().newDropTableSqlBuilder().tableName(modelDO.getTableName()).drop().buildSql();
        DbUtils.use(dataSource).exec(sql);
        //Jin 最后删除自己
        removeById(id);
        return R.ok(true);
    }

    @Override
    protected String getCacheName() {
        return "modelCache";
    }

}
