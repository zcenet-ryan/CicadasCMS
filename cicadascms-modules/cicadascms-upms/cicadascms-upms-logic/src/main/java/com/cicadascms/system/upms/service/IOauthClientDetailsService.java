package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.OauthClientDetailsDO;
import com.cicadascms.system.upms.dto.OauthClientDetailsInputDTO;
import com.cicadascms.system.upms.dto.OauthClientDetailsQueryDTO;
import com.cicadascms.system.upms.dto.OauthClientDetailsUpdateDTO;

import java.io.Serializable;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author westboy
 * @date 2019-06-19
 */
public interface IOauthClientDetailsService extends IService<OauthClientDetailsDO> {

    /**
     * 分页方法
     * @param oauthClientDetailsQueryDTO
     * @return
     */
    R page(OauthClientDetailsQueryDTO oauthClientDetailsQueryDTO);

    /**
     * 保存方法
     * @param oauthClientDetailsInputDTO
     * @return
     */
    R save(OauthClientDetailsInputDTO oauthClientDetailsInputDTO);

    /**
     * 更新方法
     * @param oauthClientDetailsUpdateDTO
     * @return
     */
    R update(OauthClientDetailsUpdateDTO oauthClientDetailsUpdateDTO);

    /**
     * 查询方法
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     * @param id
     * @return
     */
    R deleteById(Serializable id);
}
