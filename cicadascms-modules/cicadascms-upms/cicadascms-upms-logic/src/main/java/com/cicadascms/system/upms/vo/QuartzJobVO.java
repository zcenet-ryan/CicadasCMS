package com.cicadascms.system.upms.vo;

import com.cicadascms.data.domain.QuartzJobLogDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * SysQuartzJobVO对象
 * </p>
 *
 * @author jin
 * @since 2020-04-29
 */
@Data
@Accessors(chain = true)
@Schema(title = "SysQuartzJobVO对象", description = "定时任务")
public class QuartzJobVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键
     */
    @Schema(title = "1-自增主键")
    private Integer id;
    /**
     * 任务名称
     */
    @Schema(title = "2-任务名称")
    private String jobName;
    /**
     * 任务分组
     */
    @Schema(title = "3-任务分组")
    private String jobGroup;
    /**
     * 执行类
     */
    @Schema(title = "4-执行类")
    private String jobClassName;
    /**
     * cron表达式
     */
    @Schema(title = "5-cron表达式")
    private String cronExpression;
    /**
     * 任务状态
     */
    @Schema(title = "6-任务状态")
    private String triggerState;
    /**
     * 修改之前的任务名称
     */
    @Schema(title = "7-修改之前的任务名称")
    private String oldJobName;
    /**
     * 修改之前的任务分组
     */
    @Schema(title = "8-修改之前的任务分组")
    private String oldJobGroup;
    /**
     * 描述
     */
    @Schema(title = "9-描述")
    private String description;

    private List<QuartzJobLogDO> processList;
}
