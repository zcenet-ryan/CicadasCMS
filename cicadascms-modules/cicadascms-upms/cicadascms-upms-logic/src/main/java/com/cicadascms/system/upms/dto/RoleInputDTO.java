package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.RoleDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * RoleInputDTO对象
 * 角色表
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "InputRoleDTO对象")
public class RoleInputDTO extends BaseDTO<RoleInputDTO, RoleDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    @Schema(title = "0-角色id")
    private Integer roleId;
    /**
     * 租户id
     */
    @Schema(title = "1-租户id")
    private Integer tenantId;
    /**
     * 父编号
     */
    @Schema(title = "2-父编号")
    private Integer parentId;
    /**
     * 角色名称
     */
    @NotEmpty
    @Schema(title = "3-角色名称")
    private String roleName;
    /**
     * 角色标识
     */
    @NotEmpty
    @Schema(title = "4-角色标识")
    private String roleKey;
    /**
     * 菜单类型(1，系统角色，2，应用角色)
     */
    @NotNull
    @Schema(title = "5-菜单类型(1，系统角色，2，应用角色)")
    private Integer roleType;
    /**
     * 说明
     */
    @Schema(title = "6-说明")
    private String remark;

    /**
     * 权限编号
     */
    @Schema(title = "8-权限编号")
    @NotEmpty
    private String permissionIds;

    public List<String> getPermissionIdList() {
        return Fn.str2List(permissionIds);
    }

    public static Converter<RoleInputDTO, RoleDO> converter = new Converter<RoleInputDTO, RoleDO>() {
        @Override
        public RoleDO doForward(RoleInputDTO roleInputDTO) {
            return WarpsUtils.copyTo(roleInputDTO, RoleDO.class);
        }

        @Override
        public RoleInputDTO doBackward(RoleDO role) {
            return WarpsUtils.copyTo(role, RoleInputDTO.class);
        }
    };

    @Override
    public RoleDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public RoleInputDTO convertFor(RoleDO role) {
        return converter.doBackward(role);
    }
}
