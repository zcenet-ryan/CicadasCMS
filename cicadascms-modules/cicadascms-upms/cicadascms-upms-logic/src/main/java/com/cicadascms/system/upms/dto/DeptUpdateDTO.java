package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.DeptDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Set;

/**
 * <p>
 * SysDeptUpdateDTO对象
 * 机构部门
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "DeptUpdateDTO对象")
public class DeptUpdateDTO extends BaseDTO<DeptUpdateDTO, DeptDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 部门id
     */
    @Schema(title = "1-部门id")
    private Integer deptId;
    /**
     * 部门名称
     */
    @Schema(title = "2-部门名称")
    private String deptName;
    /**
     * 部门编码
     */
    @Schema(title = "3-部门编码")
    private String deptCode;
    /**
     * 父id
     */
    @Schema(title = "4-父id")
    private Integer parentId;
    /**
     * 排序字段
     */
    @Schema(title = "5-排序字段")
    private Integer sortId;
    /**
     * 备注
     */
    @Schema(title = "6-备注")
    private String remark;
    /**
     * 租户id
     */
    @Schema(title = "7-租户id")
    private Integer tenantId;

    private Set<Serializable> roleIds;

    public Integer getParentId() {
        return parentId == null ? 0: parentId;
    }

    public static Converter<DeptUpdateDTO, DeptDO> converter = new Converter<DeptUpdateDTO, DeptDO>() {
        @Override
        public DeptDO doForward(DeptUpdateDTO deptUpdateDTO) {
            return WarpsUtils.copyTo(deptUpdateDTO, DeptDO.class);
        }

        @Override
        public DeptUpdateDTO doBackward(DeptDO dept) {
            return WarpsUtils.copyTo(dept, DeptUpdateDTO.class);
        }
    };

    @Override
    public DeptDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public DeptUpdateDTO convertFor(DeptDO dept) {
        return converter.doBackward(dept);
    }
}
