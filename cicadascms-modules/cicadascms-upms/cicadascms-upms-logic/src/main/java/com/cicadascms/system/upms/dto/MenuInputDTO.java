package com.cicadascms.system.upms.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.MenuDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * MenuInputDTO对象
 * 系统菜单表
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="InputMenuDTO对象")
public class MenuInputDTO extends BaseDTO<MenuInputDTO, MenuDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 租户id
    */
    @Schema(title = "1-租户id" )
    private Integer tenantId;
    /**
    * 父菜单编号
    */
    @Schema(title = "2-父菜单编号" )
    private Integer parentId;
    /**
    * 菜单名称

    */
    @Schema(title = "3-菜单名称" )
    private String menuName;
    /**
    * 前端路由地址
    */
    @Schema(title = "4-前端路由地址" )
    private String menuPath;
    /**
    * 图标
    */
    @Schema(title = "5-图标" )
    private String menuIcon;
    /**
    * 菜单类型（1,一级菜单，2，二级菜单，3,按钮）
    */
    @Schema(title = "6-菜单类型（1,一级菜单，2，二级菜单，3,按钮）" )
    private Integer menuType;
    /**
    * 前端组件
    */
    @Schema(title = "7-前端组件" )
    private String component;
    /**
    * 跳转链接
    */
    @Schema(title = "8-跳转链接" )
    private String redirectUrl;
    /**
    * 权限标识
    */
    @Schema(title = "9-权限标识" )
    private String permissionKey;
    /**
    * 菜单状态
    */
    @Schema(title = "10-菜单状态" )
    private Boolean status;
    /**
    * 排序字段
    */
    @Schema(title = "11-排序字段" )
    private Integer sortId;


    @Schema(title = "是否隐藏")
    private Boolean hidden;

    @Schema(title = "菜单说明")
    private String remake;

    public static Converter<MenuInputDTO, MenuDO> converter = new Converter<MenuInputDTO, MenuDO>() {
        @Override
        public MenuDO doForward(MenuInputDTO menuInputDTO) {
            return WarpsUtils.copyTo(menuInputDTO, MenuDO.class);
        }

        @Override
        public MenuInputDTO doBackward(MenuDO menu) {
            return WarpsUtils.copyTo(menu, MenuInputDTO.class);
        }
    };

    @Override
    public MenuDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public MenuInputDTO convertFor(MenuDO menu) {
        return converter.doBackward(menu);
    }
}
