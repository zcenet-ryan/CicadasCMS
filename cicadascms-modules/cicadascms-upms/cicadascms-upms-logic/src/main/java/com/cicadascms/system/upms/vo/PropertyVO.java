package com.cicadascms.system.upms.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * PropertyDOVO对象
 * </p>
 *
 * @author Jin
 * @since 2021-06-06
 */
@Data
@Accessors(chain = true)
@Schema(title="PropertyDOVO对象", description="")
public class PropertyVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;
    private String propertyName;
    private String propertyValue;
    private Boolean enableHtml;

}
