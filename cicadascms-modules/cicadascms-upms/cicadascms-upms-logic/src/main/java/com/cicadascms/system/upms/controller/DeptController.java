package com.cicadascms.system.upms.controller;

import com.cicadascms.common.resp.R;
import com.cicadascms.system.upms.dto.DeptInputDTO;
import com.cicadascms.system.upms.dto.DeptQueryDTO;
import com.cicadascms.system.upms.dto.DeptUpdateDTO;
import com.cicadascms.system.upms.service.IDeptService;
import com.cicadascms.system.upms.vo.DeptVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "部门管理接口")
@RestController
@RequestMapping("/system/dept")
public class DeptController {
    @Autowired
    private IDeptService deptService;

    @Operation(summary = "部门树列表接口")
    @GetMapping("/tree")
    public R<List<DeptVO>> getTree() {
        return R.ok(deptService.getTree());
    }

    @Operation(summary = "列表查询接口")
    @GetMapping("/list")
    public R<List<DeptVO>> getList(DeptQueryDTO deptQueryDTO) {
        return R.ok(deptService.findList(deptQueryDTO));
    }

    @Operation(summary = "部门保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid DeptInputDTO deptInputDTO) {
        return deptService.save(deptInputDTO);
    }

    @Operation(summary = "部门更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid DeptUpdateDTO deptUpdateDTO) {
        return deptService.update(deptUpdateDTO);
    }

    @Operation(summary = "部门详情接口")
    @GetMapping("/{id}")
    public R<DeptVO> getById(@PathVariable String id) {
        return deptService.findById(id);
    }

    @Operation(summary = "机构部门删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable String id) {
        return deptService.deleteById(id);
    }

}
