package com.cicadascms.website.admin.dto;

import lombok.Data;
import lombok.ToString;

/**
 * ResourceFileInputDTO
 *
 * @author Jin
 */
@Data
@ToString
public class ResourceFileInputDTO {

    private String fileName;
    private String filePath;
    private String fileContent;

}
