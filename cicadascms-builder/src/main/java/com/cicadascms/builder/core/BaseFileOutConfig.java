/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.builder.core;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.ConstVal;
import com.baomidou.mybatisplus.generator.config.FileOutConfig;
import lombok.Setter;

import java.io.File;


/**
 * BaseFileOutConfig
 *
 * @author Jin
 */
public abstract class BaseFileOutConfig extends FileOutConfig {

    @Setter
    protected String module;
    @Setter
    protected String dirPath;

    public String getNonPrefixName(String tableName, String entityName) {
        if(entityName.endsWith("DO")) entityName = entityName.replace("DO","");
        if (tableName.contains("_")) {
            String prefix = StrUtil.split(tableName, "_")[0];
            return StrUtil.removePrefix(entityName, StrUtil.upperFirst(prefix));
        } else {
            return entityName;
        }
    }

    public String getLowerFirstNonPrefixName(String tableName, String entityName) {
        return StrUtil.lowerFirst(getNonPrefixName(tableName, entityName));

    }

    protected String joinPath(String parentDir, String packageName) {
        if (StringUtils.isBlank(parentDir)) {
            parentDir = System.getProperty(ConstVal.JAVA_TMPDIR);
        }
        if (!StringUtils.endsWith(parentDir, File.separator)) {
            parentDir += File.separator;
        }
        packageName = packageName.replaceAll("\\.", StringPool.BACK_SLASH + File.separator);
        return parentDir + packageName;
    }

}
