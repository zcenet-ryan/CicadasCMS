package com.cicadascms.system.upms.controller;

import com.cicadascms.common.resp.R;
import com.cicadascms.system.upms.dto.AreaInputDTO;
import com.cicadascms.system.upms.dto.AreaQueryDTO;
import com.cicadascms.system.upms.dto.AreaUpdateDTO;
import com.cicadascms.system.upms.service.IAreaService;
import com.cicadascms.system.upms.vo.AreaVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 区域 控制器
 * </p>
 *
 * @author jin
 * @since 2020-09-06
 */
@Tag(name = "区域管理接口")
@RestController
@RequestMapping("/system/area")
@AllArgsConstructor
public class AreaController {
    private final IAreaService areaService;

    @PreAuthorize(value = "hasAnyAuthority()")
    @Operation(summary = "区域列表接口")
    @GetMapping("/list")
    public R<List<AreaVO>> list(AreaQueryDTO areaQueryDTO) {
        return areaService.list(areaQueryDTO);
    }


    @Operation(summary = "区域列表树")
    @GetMapping("/tree")
    public R<List<AreaVO>> tree() {
        return R.ok(areaService.getTree());
    }

    @Operation(summary = "区域保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid AreaInputDTO areaInputDTO) {
        return areaService.save(areaInputDTO);
    }

    @Operation(summary = "区域更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid AreaUpdateDTO areaUpdateDTO) {
        return areaService.update(areaUpdateDTO);
    }

    @Operation(summary = "区域详情接口")
    @GetMapping("/{id}")
    public R<AreaVO> getById(@PathVariable Long id) {
        return areaService.findById(id);
    }

    @Operation(summary = "区域删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return areaService.deleteById(id);
    }


}
