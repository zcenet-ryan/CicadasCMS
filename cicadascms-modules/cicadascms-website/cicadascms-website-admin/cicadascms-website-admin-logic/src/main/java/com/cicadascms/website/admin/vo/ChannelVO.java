package com.cicadascms.website.admin.vo;

import com.cicadascms.common.tree.TreeNode;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * ChannelVO对象
 * </p>
 *
 * @author Jin
 * @since 20201102
 */
@Data
@Accessors(chain = true)
@Schema(title = "ChannelVO", description = "栏目")
public class ChannelVO implements Serializable, TreeNode<ChannelVO> {

    private static final long serialVersionUID = 1L;

    private Integer channelId;
    /**
     * 站点编号
     */
    @Schema(title = "站点编号")
    private Integer siteId;

    /**
     * 域名
     */
    @Schema(title = "栏目域名")
    private String domain;

    /**
     * 分类明细
     */
    @Schema(title = "分类明细")
    private String channelName;
    /**
     * 栏目模型编号
     */
    @Schema(title = "栏目模型编号")
    private Integer channelModelId;
    /**
     * 内容模型编号
     */
    @Schema(title = "内容模型编号")
    private String contentModelIds;
    /**
     * 栏目路径
     */
    @Schema(title = "栏目路径")
    private String channelUrlPath;
    /**
     * 父类编号
     */
    @Schema(title = "父类编号")
    private Long parentId;
    /**
     * 单页栏目（0：不是，1：是）
     */
    @Schema(title = "单页栏目（0：不是，1：是）")
    private Boolean isAlone;
    /**
     * 单页内容
     */
    @Schema(title = "单页内容")
    private String aloneContent;
    /**
     * 首页视图模板
     */
    @Schema(title = "首页视图模板")
    private String indexView;
    /**
     * 列表页视图模板
     */
    @Schema(title = "列表页视图模板")
    private String listView;
    /**
     * 内容页视图模板
     */
    @Schema(title = "内容页视图模板")
    private String contentView;
    /**
     * 导航
     */
    @Schema(title = "导航")
    private Boolean isNav;
    /**
     * 外链地址
     */
    @Schema(title = "外链地址")
    private String url;
    /**
     * 是否有子类
     */
    @Schema(title = "是否有子类")
    private Boolean hasChildren;
    /**
     * 栏目分页数量
     */
    @Schema(title = "栏目分页数量")
    private Integer pageSize;
    /**
     * 当前栏目下的是否支持全文搜索
     */
    @Schema(title = "当前栏目下的是否支持全文搜索")
    private Boolean allowSearch;
    /**
     * 栏目分类
     */
    @Schema(title = "栏目分类")
    private Integer channelType;
    /**
     * 栏目图标
     */
    @Schema(title = "栏目图标")
    private String channelIcon;
    private Integer sortId;
    /**
     * 创建人
     */
    @Schema(title = "创建人")
    private Integer createUser;
    /**
     * 创建时间
     */
    @Schema(title = "创建时间")
    private LocalDateTime createTime;
    /**
     * 更新用户
     */
    @Schema(title = "更新用户")
    private Integer updateUser;
    /**
     * 更新时间
     */
    @Schema(title = "更新时间")
    private LocalDateTime updateTime;

    /**
     * 扩展字段
     */
    @Schema(title = "扩展字段")
    private List<ModelFieldValueVO> ext;

    String parentName;
    List<ChannelVO> children;

    public void setChildren(List<ChannelVO> children) {
        this.children = children;
    }

    @Override
    public Serializable getCurrentNodeId() {
        return getChannelId();
    }

    @Override
    public Serializable getParentNodeId() {
        return getParentId();
    }

}
