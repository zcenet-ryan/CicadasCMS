package com.cicadascms.system.upms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.dict.DictManager;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.DictDO;
import com.cicadascms.system.upms.dto.DictInputDTO;
import com.cicadascms.system.upms.dto.DictQueryDTO;
import com.cicadascms.system.upms.dto.DictUpdateDTO;
import com.cicadascms.system.upms.service.IDictService;
import com.cicadascms.system.upms.vo.DictDetailsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@Api(tags = "字典管理接口")
@RestController
@RequestMapping("/system/dict")
public class DictController {
    @Autowired
    private IDictService dictService;
    @Autowired
    private DictManager dictManager;

    @Operation(summary = "字典列表接口")
    @GetMapping("/list")
    public R<List<DictDO>> getList(DictQueryDTO dictQueryDTO) {
        return dictService.findList(dictQueryDTO);
    }

    @Operation(summary = "字典列表接口")
    @GetMapping("/data/{dictCode}")
    public R<List<DictDetailsVO>> data(@PathVariable String dictCode) {
        return R.ok(dictManager.getOptionalDictData(dictCode)
                .orElseThrow(new ServiceException("字典[" + dictCode + "]不存在！"))
                .stream()
                .map(dict -> {
                    DictDetailsVO dictDetailsVo = new DictDetailsVO();
                    dictDetailsVo.setName(dict.getDictName());
                    dictDetailsVo.setValue(dict.getDictValue());
                    return dictDetailsVo;
                }).collect(Collectors.toList()));
    }

    @Operation(summary = "字典列表接口")
    @GetMapping("/page")
    public R<Page<DictDO>> getPage(DictQueryDTO dictQueryDTO) {
        return dictService.page(dictQueryDTO);
    }

    @Operation(summary = "字典保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid DictInputDTO dictInputDTO) {
        return dictService.save(dictInputDTO);
    }

    @Operation(summary = "字典更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid DictUpdateDTO dictUpdateDTO) {
        return dictService.update(dictUpdateDTO);
    }

    @Operation(summary = "字典详情接口")
    @GetMapping("/{id}")
    public R<DictDO> getById(@PathVariable Long id) {
        return dictService.findById(id);
    }

    @Operation(summary = "字典删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return dictService.deleteById(id);
    }


}
