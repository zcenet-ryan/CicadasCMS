package com.cicadascms.website.admin.wrapper;

import com.cicadascms.website.admin.service.IAdminSiteService;
import com.cicadascms.website.admin.vo.SiteVO;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.SiteDO;
import org.springframework.util.Assert;

public class SiteWrapper implements BaseWrapper<SiteDO, SiteVO> {

    private final static IAdminSiteService siteService;

    static {
        siteService = SpringContextUtils.getBean(IAdminSiteService.class);
    }

    public static SiteWrapper newBuilder() {
        return new SiteWrapper();
    }

    @Override
    public SiteVO entityVO(SiteDO entity) {
        Assert.isTrue(Fn.isNotNull(entity), SiteDO.class.getName() + " 对象不存在！");
        return  WarpsUtils.copyTo(entity, SiteVO.class);
    }
}
