package com.cicadascms.system.upms.vo;

import com.cicadascms.common.tree.TreeNode;
import com.cicadascms.data.domain.MenuDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Schema(title = "MenuVo", description = "SysMenu对象")
@Data
@EqualsAndHashCode(callSuper = true)
public class MenuVO extends MenuDO implements TreeNode<MenuVO> {
    private Boolean hidden;
    private String parentName;
    private List<MenuVO> children;

    @Override
    public Serializable getCurrentNodeId() {
        return this.getMenuId();
    }

    @Override
    public Serializable getParentNodeId() {
        return this.getParentId();
    }

}
