package com.cicadascms.website.admin.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * ContentVO对象
 * </p>
 *
 * @author Jin
 * @since 20201102
 */
@Data
@Accessors(chain = true)
@Schema(title = "ContentVO", description = "内容")
public class ContentVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 内容编号
     */
    @Schema(title = "内容编号")
    private Integer contentId;
    /**
     * 站点编号
     */
    @Schema(title = "站点编号")
    private Integer siteId;
    /**
     * 栏目编号
     */
    @Schema(title = "栏目编号")
    private Integer channelId;
    /**
     * 模型编号
     */
    @Schema(title = "模型编号")
    private Integer modelId;
    /**
     * 标题
     */
    @Schema(title = "标题")
    private String title;
    /**
     * 副标题
     */
    @Schema(title = "副标题")
    private String subTitle;
    /**
     * 作者
     */
    @Schema(title = "作者")
    private String author;
    /**
     * 页面关键字
     */
    @Schema(title = "页面关键字")
    private Integer keywords;
    /**
     * 页面描述
     */
    @Schema(title = "页面描述")
    private Integer description;
    /**
     * 录入时间
     */
    @Schema(title = "录入时间")
    private LocalDateTime inputTime;
    /**
     * 更新时间
     */
    @Schema(title = "更新时间")
    private Integer updateTime;
    /**
     * 内容状态
     */
    @Schema(title = "内容状态")
    private Integer state;
    /**
     * 来源
     */
    @Schema(title = "来源")
    private String source;
    /**
     * 原文地址
     */
    @Schema(title = "原文地址")
    private String sourceUrl;
    /**
     * 封面图片
     */
    @Schema(title = "封面图片")
    private String thumb;
    /**
     * 浏览数量
     */
    @Schema(title = "浏览数量")
    private Integer viewNum;
    /**
     * 价格
     */
    @Schema(title = "价格")
    private Integer price;
    /**
     * 付费阅读
     */
    @Schema(title = "付费阅读")
    private Boolean paidReading;
    /**
     * 扩展字段
     */
    @Schema(title = "扩展字段")
    private List<ModelFieldValueVO> ext;

}
