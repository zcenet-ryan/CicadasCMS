package com.cicadascms.website.admin.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.support.datamodel.modelfield.ModelFieldProp;
import com.cicadascms.support.datamodel.modelfield.ModelFieldRule;
import com.cicadascms.support.datamodel.modelfield.ModelFieldValue;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * ModelFieldUpdateDTO
 * 模型字段
 * </p>
 *
 * @author Jin
 * @since 202102
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="ModelFieldUpdateDTO")
public class ModelFieldUpdateDTO extends BaseDTO<ModelFieldUpdateDTO, ModelFieldDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 主键
    */
    @Schema(title = "主键" )
    private Integer fieldId;
    /**
    * 模型字段
    */
    @Schema(title = "模型字段" )
    private Integer modelId;
    /**
    * 字段名称
    */
    @Schema(title = "字段名称" )
    private String fieldName;
    /**
    * 字段类型
    */
    @Schema(title = "字段类型" )
    private Integer fieldType;
    /**
    * 数据库字段类型
    */
    @Schema(title = "数据库字段类型" )
    private Integer columnType;
    /**
     * 字段属性
     */
    @Schema(title = "字段规则")
    private ModelFieldProp<ModelFieldRule, ModelFieldValue> fieldProp;
    /**
    * 是否检索字段
    */
    @Schema(title = "是否检索字段" )
    private Boolean isSearchField;
    /**
    * 字段描述
    */
    @Schema(title = "字段描述" )
    private String des;

    public static Converter<ModelFieldUpdateDTO, ModelFieldDO> converter = new Converter<ModelFieldUpdateDTO, ModelFieldDO>() {
        @Override
        public ModelFieldDO doForward(ModelFieldUpdateDTO modelFieldUpdateDTO) {
            ModelFieldDO modelFieldDO = WarpsUtils.copyTo(modelFieldUpdateDTO, ModelFieldDO.class);
            assert modelFieldDO != null;
            modelFieldDO.setFieldConfig(Fn.toJson(modelFieldUpdateDTO.getFieldProp()));
            return modelFieldDO;
        }

        @Override
        public ModelFieldUpdateDTO doBackward(ModelFieldDO modelFieldDO) {
            return WarpsUtils.copyTo(modelFieldDO, ModelFieldUpdateDTO.class);
        }
    };

    @Override
    public ModelFieldDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ModelFieldUpdateDTO convertFor(ModelFieldDO modelFieldDO) {
        return converter.doBackward(modelFieldDO);
    }
}
