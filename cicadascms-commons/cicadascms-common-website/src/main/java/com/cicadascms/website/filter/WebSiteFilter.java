/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.website.filter;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cicadascms.common.enums.ActiveStateEnum;
import com.cicadascms.common.exception.FrontNotFoundException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.ControllerUtil;
import com.cicadascms.data.domain.ChannelDO;
import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.data.mapper.ChannelMapper;
import com.cicadascms.data.mapper.SiteMapper;
import com.cicadascms.website.WebSiteContextHolder;
import lombok.AllArgsConstructor;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * WebSiteFilter
 *
 * @author Jin
 */
@Order(0)
@Component
@AllArgsConstructor
@WebFilter(filterName = "WebSiteFilter", urlPatterns = {"/*"})
public class WebSiteFilter implements Filter {
    private final SiteMapper siteMapper;
    private final ChannelMapper channelMapper;

    private final static AntPathMatcher pathMatcher = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        String requestPath = request.getServletPath();

        if (pathMatcher.match("/favicon.ico", requestPath)) {
            request.getRequestDispatcher("/static/favicon.ico").forward(request, response);
            return;
        }

        if (pathMatcher.match("/system/**", requestPath) || pathMatcher.match("/oauth/**", requestPath)) {
            chain.doFilter(req, res);
            return;
        }

        String domain = ControllerUtil.getDomain(request);
        SiteDO siteDO = siteMapper.selectOne(new LambdaQueryWrapper<SiteDO>()
                .eq(SiteDO::getStatus, ActiveStateEnum.启用.getCode())
                .eq(SiteDO::getDomain, domain)
        );

        if (Fn.isNull(siteDO)) {
            ChannelDO channelDO = channelMapper.selectOne(new LambdaQueryWrapper<ChannelDO>()
                    .eq(ChannelDO::getDomain, domain));
            if (Fn.isNotNull(channelDO)) {
                siteDO = siteMapper.selectById(channelDO.getSiteId());
                WebSiteContextHolder.set(siteDO);
                if (Fn.equal("/", requestPath)) {
                    request.getRequestDispatcher(channelDO.getChannelUrlPath()).forward(request, response);
                }
            } else {
                String siteId = request.getParameter("siteId");
                if (Fn.isEmpty(siteId)) {
                    siteDO = siteMapper.selectOne(new LambdaQueryWrapper<SiteDO>()
                            .eq(SiteDO::getDomain, domain)
                            .eq(SiteDO::getStatus, ActiveStateEnum.启用.getCode())
                            .eq(SiteDO::getIsDefault, true)
                    );
                } else {
                    siteDO = siteMapper.selectById(siteId);
                }
            }
        }

        if (Fn.isNull(siteDO)) {
            throw new FrontNotFoundException("站点不存在！");
        }

        WebSiteContextHolder.set(siteDO);
        if (pathMatcher.match("/search", requestPath))
            request.getRequestDispatcher("/" + siteDO.getSiteDir() + "/search").forward(request, response);
        chain.doFilter(req, res);
        WebSiteContextHolder.remove();

    }
}
