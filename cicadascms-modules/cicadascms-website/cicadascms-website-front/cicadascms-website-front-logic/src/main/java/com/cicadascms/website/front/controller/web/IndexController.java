package com.cicadascms.website.front.controller.web;

import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.support.annotation.CurrentWebSite;
import com.cicadascms.website.util.WebSiteViewUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class IndexController {

    private final static String DEFAULT_INDEX_VIEW_NAME = "/index";

    @GetMapping("/")
    public String index(@CurrentWebSite SiteDO siteDO, Model model) {
        model.addAttribute("site", siteDO);
        return  WebSiteViewUtil.indexViewRender(siteDO.getPcTemplateDir(), siteDO.getMobileTemplateDir(), DEFAULT_INDEX_VIEW_NAME);
    }

}
