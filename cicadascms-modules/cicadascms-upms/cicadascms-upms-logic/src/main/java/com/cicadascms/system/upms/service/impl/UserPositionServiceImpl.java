package com.cicadascms.system.upms.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.data.domain.PositionDO;
import com.cicadascms.data.domain.UserPositionDO;
import com.cicadascms.data.mapper.SysUserPositionMapper;
import com.cicadascms.system.upms.service.IPositionService;
import com.cicadascms.system.upms.service.IUserPositionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户部门关联表 service impl class
 * </p>
 *
 * @author westboy
 * @date 2019-08-21
 */
@Service
@AllArgsConstructor
public class UserPositionServiceImpl extends BaseService<SysUserPositionMapper, UserPositionDO> implements IUserPositionService {
    private final IPositionService positionService;


    @Override
    public List<UserPositionDO> findByUserId(Integer uid) {
        return baseMapper.selectList(getLambdaQueryWrapper().eq(UserPositionDO::getUserId, uid));
    }


    @Override
    public void updateUserPosition(Integer uid, List<String> postIds) {
        if (CollectionUtil.isNotEmpty(postIds)) {
            LambdaQueryWrapper<UserPositionDO> lambdaQueryWrapper = getLambdaQueryWrapper().eq(UserPositionDO::getUserId, uid);
            Integer count = baseMapper.selectCount(lambdaQueryWrapper);

            if (count > 0) {
                baseMapper.delete(lambdaQueryWrapper);
            }

            postIds.forEach(postId -> {
                PositionDO position = positionService.getById(postId);
                if (position != null) {
                    UserPositionDO userPosition = new UserPositionDO();
                    userPosition.setPostId(position.getId());
                    userPosition.setUserId(uid);
                    baseMapper.insert(userPosition);
                }
            });
        }
    }


    @Override
    protected String getCacheName() {
        return "userPositionCache";
    }
}
