/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.common.base;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * BaseDTO
 *
 * @author Jin
 */
public abstract class BaseDTO<D, E>{

    public abstract E convertToEntity();

    public abstract D convertFor(E e);

    protected Page<E> getPage(long current,long size){
        return new Page<>(current, size);
    }

}
