package com.cicadascms.system.upms.wrapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.QuartzJobDO;
import com.cicadascms.data.domain.QuartzJobLogDO;
import com.cicadascms.data.mapper.QuartzJobLogMapper;
import com.cicadascms.system.upms.service.IQuartzJobService;
import com.cicadascms.system.upms.vo.QuartzJobVO;

/**
 * <p>
 * 定时任务 包装类
 * </p>
 *
 * @author Jin
 * @since 2021-06-08
 */
public class QuartzJobWrapper implements BaseWrapper<QuartzJobDO, QuartzJobVO> {

    private final static IQuartzJobService quartzJobService;
    private final static QuartzJobLogMapper quartzJobLogMapper;

    static {
        quartzJobService = SpringContextUtils.getBean(IQuartzJobService.class);
        quartzJobLogMapper = SpringContextUtils.getBean(QuartzJobLogMapper.class);
    }

    public static QuartzJobWrapper newBuilder() {
        return new QuartzJobWrapper();
    }

    @Override
    public QuartzJobVO entityVO(QuartzJobDO entity) {
        QuartzJobVO quartzJobVO = WarpsUtils.copyTo(entity, QuartzJobVO.class);
        IPage<QuartzJobLogDO> page = quartzJobLogMapper.selectPage(new Page<>(1, 10),
                new LambdaQueryWrapper<QuartzJobLogDO>()
                        .eq(QuartzJobLogDO::getJobName, quartzJobVO.getJobName())
                        .orderByDesc(QuartzJobLogDO::getFireTime)
        );
        quartzJobVO.setProcessList(page.getRecords());
        return quartzJobVO;
    }
}
