package com.cicadascms.system.upms.vo;

import com.cicadascms.common.dict.Dict;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * SysDictVO对象
 * </p>
 *
 * @author jin
 * @since 2020-04-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@Schema(title = "SysDictVO对象", description = "字典表")
public class DictVO extends Dict {

}
