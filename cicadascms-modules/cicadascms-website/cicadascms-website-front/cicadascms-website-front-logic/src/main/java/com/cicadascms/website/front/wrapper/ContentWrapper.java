package com.cicadascms.website.front.wrapper;

import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.exception.FrontNotFoundException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.ContentDO;
import com.cicadascms.data.domain.ModelDO;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.website.front.service.IContentService;
import com.cicadascms.website.front.service.IModelFieldService;
import com.cicadascms.website.front.service.IModelService;
import com.cicadascms.website.front.vo.ContentVO;
import com.cicadascms.support.datamodel.modelfield.ModelFieldValue;
import lombok.val;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 内容包装类
 *
 * @author Jin
 */
public class ContentWrapper implements BaseWrapper<ContentDO, ContentVO> {

    private final static IContentService contentService;
    private final static IModelService modelService;
    private final static IModelFieldService modelFieldService;

    static {
        contentService = SpringContextUtils.getBean(IContentService.class);
        modelService = SpringContextUtils.getBean(IModelService.class);
        modelFieldService = SpringContextUtils.getBean(IModelFieldService.class);
    }

    public static ContentWrapper newBuilder() {
        return new ContentWrapper();
    }

    @Override
    public ContentVO entityVO(ContentDO entity) {
        ContentVO contentVO = WarpsUtils.copyTo(entity, ContentVO.class);
        if (Fn.isNull(contentVO)) throw new FrontNotFoundException("内容已被删除或不存在！");
        ModelDO modelDO = modelService.getModelById(contentVO.getModelId());
        if (Fn.isNull(modelDO)) throw new FrontNotFoundException("模型已被删除或不存在！");
        List<ModelFieldDO> modelFieldDOList = modelFieldService.findByModelId(modelDO.getModelId());
        if (Fn.isEmpty(modelFieldDOList)) throw new FrontNotFoundException("模型已被删除或不存在！");
        Map<String, Object> contentExtendFieldValue = contentService.findByTableNameAndContentId(modelFieldDOList
                        .stream()
                        .parallel()
                        .map(ModelFieldDO::getFieldName)
                        .collect(Collectors.joining(",")),
                modelDO.getTableName(), contentVO.getContentId());
        //这只内容字段变量
        if (Fn.isNotEmpty(contentExtendFieldValue)) {
            Map<String, Object> newExtendFieldValue = new HashMap<>();
            contentExtendFieldValue.forEach((key, value) -> {
                val modelFieldValue = Fn.readValue(value.toString(), ModelFieldValue.class);
                newExtendFieldValue.put(key, modelFieldValue.getValue());
            });
            contentVO.setExt(newExtendFieldValue);
        }
        return contentVO;
    }

}
