package com.cicadascms.system.upms.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.system.upms.dto.PropertyInputDTO;
import com.cicadascms.system.upms.dto.PropertyQueryDTO;
import com.cicadascms.system.upms.dto.PropertyUpdateDTO;
import com.cicadascms.system.upms.service.IPropertyService;
import com.cicadascms.system.upms.vo.PropertyVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * <p>
 *  控制器
 * </p>
 *
 * @author Jin
 * @since 2021-06-06
 */
@Tag(name = "参数管理接口")
@RestController
@RequestMapping("/system/property")
@AllArgsConstructor
public class PropertyController {
    private final IPropertyService propertyService;

    @Operation(summary = "分页接口")
    @GetMapping("/page")
    public R<Page<PropertyVO>> page(PropertyQueryDTO propertyQueryDTO) {
        return propertyService.page(propertyQueryDTO);
    }

    @Operation(summary = "保存接口")
    @PostMapping
    public R<Boolean> save(@Valid @RequestBody PropertyInputDTO propertyInputDTO) {
        return propertyService.save(propertyInputDTO);
    }

    @Operation(summary = "更新接口")
    @PutMapping
    public R<Boolean> updateById(@Valid @RequestBody PropertyUpdateDTO propertyUpdateDTO) {
        return propertyService.update(propertyUpdateDTO);
    }

    @Operation(summary = "详情接口")
    @GetMapping("/{id}")
    public R<PropertyVO> getById(@PathVariable Long id) {
        return propertyService.findById(id);
    }

    @Operation(summary = "删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return propertyService.deleteById(id);
    }


}
