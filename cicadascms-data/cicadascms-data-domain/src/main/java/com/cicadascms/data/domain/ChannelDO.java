/*
 * Copyright (c) 2021 CicadasCMS
 * CicadasCMS is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cicadascms.common.base.BaseDO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 内容分类表
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_channel")
public class ChannelDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(value = "channel_id", type = IdType.AUTO)
    private Integer channelId;

    /**
     * 站点编号
     */
    private Integer siteId;

    /**
     * 分类明细
     */
    private String channelName;

    /**
     * 域名
     */
    private String domain;

    /**
     * 栏目模型编号
     */
    private Integer channelModelId;

    /**
     * 内容模型编号
     */
    private String contentModelIds;

    /**
     * 栏目路径
     */
    private String channelUrlPath;

    /**
     * 父类编号
     */
    private Integer parentId;

    /**
     * 单页栏目（0：不是，1：是）
     */
    private Boolean isAlone;

    /**
     * 单页内容
     */
    private String aloneContent;

    /**
     * 首页视图模板
     */
    private String channelView;

    /**
     * 导航
     */
    private Boolean isNav;

    /**
     * 外链地址
     */
    private String url;

    /**
     * 是否有子类
     */
    private Boolean hasChildren;

    /**
     * 栏目分页数量
     */
    private Integer pageSize;
    /**
     * 栏目分类
     */
    private Integer channelType;

    /**
     * 栏目图标
     */
    private String channelIcon;

    private Integer sortId;
}
