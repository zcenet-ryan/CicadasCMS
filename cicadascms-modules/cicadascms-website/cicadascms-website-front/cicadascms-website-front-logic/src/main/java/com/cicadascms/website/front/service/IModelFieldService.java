package com.cicadascms.website.front.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.data.domain.ModelFieldDO;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 模型字段 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface IModelFieldService extends IService<ModelFieldDO> {


    /**
     * @param modelId
     * @return
     */
    List<ModelFieldDO> findByModelId(Serializable modelId);
}
