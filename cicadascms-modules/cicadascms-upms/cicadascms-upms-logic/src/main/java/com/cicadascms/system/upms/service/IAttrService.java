package com.cicadascms.system.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.AttrDO;
import com.cicadascms.system.upms.dto.AttrInputDTO;
import com.cicadascms.system.upms.dto.AttrQueryDTO;
import com.cicadascms.system.upms.dto.AttrUpdateDTO;

import java.io.Serializable;

/**
 * <p>
 * 附件 服务类
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
public interface IAttrService extends IService<AttrDO> {

    /**
     * 分页方法
     * @param attrQueryDTO
     * @return
     */
    R page(AttrQueryDTO attrQueryDTO);

    /**
     * 保存方法
     * @param attrInputDTO
     * @return
     */
    R save(AttrInputDTO attrInputDTO);

    /**
     * 更新方法
     * @param attrUpdateDTO
     * @return
     */
    R update(AttrUpdateDTO attrUpdateDTO);

    /**
     * 查询方法
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     * @param id
     * @return
     */
    R deleteById(Serializable id);
}
