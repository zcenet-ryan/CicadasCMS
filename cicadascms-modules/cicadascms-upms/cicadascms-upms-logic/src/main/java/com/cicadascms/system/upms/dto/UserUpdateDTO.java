package com.cicadascms.system.upms.dto;


import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.UserDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * SysUserUpdateDTO对象
 * 基础账户表
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "UserUpdateDTO对象")
public class UserUpdateDTO extends BaseDTO<UserUpdateDTO, UserDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 账号id
     */
        @Schema(title = "1-账号id")
    private Integer userId;
    /**
     * 邮箱
     */
        @Schema(title = "2-邮箱")
    private String email;
    /**
     * 手机号
     */
        @Schema(title = "3-手机号")
    private String phone;
    /**
     * 登陆名
     */
        @Schema(title = "4-登陆名")
    private String username;
    /**
     * 密码
     */
        @Schema(title = "5-密码")
    private String password;
    /**
     * 用户类型（1.系统管理员；2,入驻药店；3.平台管理员；4.平台会员，5.入驻医生）
     */
        @Schema(title = "6-用户类型（1.系统管理员；2,入驻药店；3.平台管理员；4.平台会员，5.入驻医生）")
    private Integer userType;
    /**
     * 租户id
     */
        @Schema(title = "7-租户id")
    private Integer tenantId;
    /**
     * 状态 1:enable, 0:disable, -1:deleted
     */
        @Schema(title = "8-状态 1:enable, 0:disable, -1:deleted")
    private Integer status;

    private String realName;

    private String avatar;

    /**
     * 角色编号
     */
        @Schema(title = "角色编号")
    private String selectedRoleIdsStr;

    /**
     * 职位编号
     */
        @Schema(title = "职位编号")
    private String selectedPostIdsStr;

    /**
     * 部门编号
     */
        @Schema(title = "部门编号")
    private String selectedDeptIdsStr;


    public List<String> getRoleIdList() {
        if (Fn.isEmpty(selectedRoleIdsStr)) {
            return null;
        }
        return Fn.str2List(selectedRoleIdsStr);
    }

    public List<String> getPostIdList() {
        if (Fn.isEmpty(selectedPostIdsStr)) {
            return null;
        }
        return Fn.str2List(selectedPostIdsStr);
    }

    public List<String> getDeptIdList() {
        if (Fn.isEmpty(selectedDeptIdsStr)) {
            return null;
        }
        return Fn.str2List(selectedDeptIdsStr);
    }

    public static Converter<UserUpdateDTO, UserDO> converter = new Converter<UserUpdateDTO, UserDO>() {
        @Override
        public UserDO doForward(UserUpdateDTO userUpdateDTO) {
            return WarpsUtils.copyTo(userUpdateDTO, UserDO.class);
        }

        @Override
        public UserUpdateDTO doBackward(UserDO user) {
            return WarpsUtils.copyTo(user, UserUpdateDTO.class);
        }
    };

    @Override
    public UserDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public UserUpdateDTO convertFor(UserDO user) {
        return converter.doBackward(user);
    }
}
