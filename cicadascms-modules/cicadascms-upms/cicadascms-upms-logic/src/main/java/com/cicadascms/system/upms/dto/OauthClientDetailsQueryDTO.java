package com.cicadascms.system.upms.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.OauthClientDetailsDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * OauthClientDetailsQueryDTO对象
 * oauth客户端
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title="OauthClientDetailsQueryDTO对象")
public class OauthClientDetailsQueryDTO extends BaseDTO<OauthClientDetailsQueryDTO, OauthClientDetailsDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(title = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @Schema(title = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @Schema(title = "3-降序排序字段 多个字段用英文逗号隔开")
    private String descs;

    @Schema(title = "4-升序排序字段  多个字段用英文逗号隔开")
    private String ascs;

    /**
    * 应用标识
    */
    @Schema(title = "3-应用标识" )
    private String clientId;
    /**
    * 资源限定串(逗号分割)
    */
    @Schema(title = "4-资源限定串(逗号分割)" )
    private String resourceIds;
    /**
    * 应用密钥(bcyt) 加密
    */
    @Schema(title = "5-应用密钥(bcyt) 加密" )
    private String clientSecret;
    /**
    * 应用密钥(明文)
    */
    @Schema(title = "6-应用密钥(明文)" )
    private String clientSecretStr;
    /**
    * 范围
    */
    @Schema(title = "7-范围" )
    private String scope;
    /**
    * 5种oauth授权方式(authorization_code,password,refresh_token,client_credentials)
    */
    @Schema(title = "8-5种oauth授权方式(authorization_code,password,refresh_token,client_credentials)" )
    private String authorizedGrantTypes;
    /**
    * 回调地址
    */
    @Schema(title = "9-回调地址" )
    private String webServerRedirectUri;
    /**
    * 权限
    */
    @Schema(title = "10-权限" )
    private String authorities;
    /**
    * access_token有效期
    */
    @Schema(title = "11-access_token有效期" )
    private Integer accessTokenValidity;
    /**
    * refresh_token有效期
    */
    @Schema(title = "12-refresh_token有效期" )
    private Integer refreshTokenValidity;
    /**
    * {}
    */
    @Schema(title = "13-{}" )
    private String additionalInformation;
    /**
    * 是否自动授权 是-true
    */
    @Schema(title = "14-是否自动授权 是-true" )
    private String autoapprove;

    public Page<OauthClientDetailsDO> page() {
        Page<OauthClientDetailsDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<OauthClientDetailsQueryDTO, OauthClientDetailsDO> converter = new Converter<OauthClientDetailsQueryDTO, OauthClientDetailsDO>() {
        @Override
        public OauthClientDetailsDO doForward(OauthClientDetailsQueryDTO oauthClientDetailsQueryDTO) {
            return WarpsUtils.copyTo(oauthClientDetailsQueryDTO, OauthClientDetailsDO.class);
        }

        @Override
        public OauthClientDetailsQueryDTO doBackward(OauthClientDetailsDO oauthClientDetails) {
            return WarpsUtils.copyTo(oauthClientDetails, OauthClientDetailsQueryDTO.class);
        }
    };

    @Override
    public OauthClientDetailsDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public OauthClientDetailsQueryDTO convertFor(OauthClientDetailsDO oauthClientDetails) {
        return converter.doBackward(oauthClientDetails);
    }
}
