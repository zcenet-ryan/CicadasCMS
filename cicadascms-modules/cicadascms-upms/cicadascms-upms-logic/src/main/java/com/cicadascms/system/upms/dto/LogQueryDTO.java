package com.cicadascms.system.upms.dto;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.LogDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * LogQueryDTO对象
 * 日志表
 * </p>
 *
 * @author jin
 * @since 2020-05-05
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@Schema(title = "LogQueryDTO对象")
public class LogQueryDTO extends BaseDTO<LogQueryDTO, LogDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(title = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @Schema(title = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @Schema(title = "3-降序排序字段 多个字段用英文逗号隔开")
    private String descs;

    @Schema(title = "4-升序排序字段  多个字段用英文逗号隔开")
    private String ascs;

    /**
     * 日志类型
     */
    @Schema(title = "3-日志类型")
    private String type;
    /**
     * 日志标题
     */
    @Schema(title = "4-日志标题")
    private String title;
    /**
     * 服务ID
     */
    @Schema(title = "5-服务ID")
    private String clientId;
    /**
     * 创建者
     */
    @Schema(title = "6-创建者")
    private String createBy;
    /**
     * 创建时间
     */
    @Schema(title = "7-创建时间")
    private LocalDateTime createTime;
    /**
     * 操作IP地址
     */
    @Schema(title = "8-操作IP地址")
    private String remoteAddr;
    /**
     * 用户代理
     */
    @Schema(title = "9-用户代理")
    private String userAgent;
    /**
     * 请求URI
     */
    @Schema(title = "10-请求URI")
    private String requestUri;
    /**
     * 操作方式
     */
    @Schema(title = "11-操作方式")
    private String method;
    /**
     * 操作提交的数据
     */
    @Schema(title = "12-操作提交的数据")
    private String params;
    /**
     * 执行时间
     */
    @Schema(title = "13-执行时间")
    private String time;
    /**
     * 异常信息
     */
    @Schema(title = "14-异常信息")
    private String exception;
    /**
     * 所属租户
     */
    @Schema(title = "15-所属租户")
    private Integer tenantId;
    /**
     * 请求时长
     */
    @Schema(title = "16-请求时长")
    private String processTime;

    public Page<LogDO> page() {
        Page<LogDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<LogQueryDTO, LogDO> converter = new Converter<LogQueryDTO, LogDO>() {
        @Override
        public LogDO doForward(LogQueryDTO logQueryDTO) {
            return WarpsUtils.copyTo(logQueryDTO, LogDO.class);
        }

        @Override
        public LogQueryDTO doBackward(LogDO log) {
            return WarpsUtils.copyTo(log, LogQueryDTO.class);
        }
    };

    @Override
    public LogDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public LogQueryDTO convertFor(LogDO log) {
        return converter.doBackward(log);
    }
}
