package com.cicadascms.support.spring.interceptor;

import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SecurityUtils;
import com.cicadascms.security.LoginUserDetails;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

@Component
public class PermissionInterceptor implements HandlerInterceptor {
    private final AntPathMatcher pathMatcher = new AntPathMatcher();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestUri = request.getRequestURI();
        String method = request.getMethod();

        if (HttpMethod.OPTIONS.matches(method)) {
            return true;
        }

        LoginUserDetails loginUserDetails = (LoginUserDetails) SecurityUtils.getCurrentLoginUser();

        if (Fn.isNull(loginUserDetails)) {
            throw new UsernameNotFoundException("未获取当前用户信息！");
        }

        Set<String> allowAccessUrls = loginUserDetails.getAllowAccessUrls();

        if (Fn.isEmpty(allowAccessUrls)) {
            throw new UsernameNotFoundException("未获取可访问的接口地址！");
        }

        //检查当前用户是否有该接口的访问权限
        if (allowAccessUrls.stream().parallel().anyMatch(allowAccessUrl -> pathMatcher.match(allowAccessUrl, requestUri))) {
            return true;
        } else {
            throw new UsernameNotFoundException("没有此接口[" + requestUri + "]的访问权限！");
        }
    }
}
